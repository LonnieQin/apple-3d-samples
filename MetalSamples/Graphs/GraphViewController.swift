//
//  GraphViewController.swift
//  ThreeDSamples
//
//  Created by lonnie on 2017/2/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit

class GraphViewController: UIViewController {
    enum Option:String {
        case RotateKey = "RotateKey"
        case ShouldRain = "ShouldRain"

    }
    class func instance(geometry:SCNGeometry,options:[Option:Any] = [:])->GraphViewController {
        let c = GraphViewController(nibName: "GraphViewController", bundle: nil)
        c.options = options
        c.geometry = geometry
        return c
    }
    class func instance(node:SCNNode,options:[Option:Any] = [:])->GraphViewController {
        let c = GraphViewController(nibName: "GraphViewController", bundle: nil)
        c.options = options
        c.node = node
        return c
    }
    var geometry:SCNGeometry?
    
    var node:SCNNode?
    var options:[Option:Any] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hidesBarsOnTap = true 
        let graphView = (self.view as! GraphView)
        options.forEach { (option,value) in
            switch option {
            case .RotateKey:
                graphView.shouldRotate = (value as! Bool)
            case .ShouldRain:
                graphView.scene!.rootNode.addParticleSystem(SCNParticleSystem(named: "rain", inDirectory: nil)!)
            }
        }
        if geometry != nil {
            graphView.geometry = geometry!
        }
        if node != nil {
            graphView.graphNode = node!
        }
    }

}
