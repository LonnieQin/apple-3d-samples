//
//  GraphView.m
//  DRMeshGeometry
//
//  Created by David Rönnqvist on 5/26/13.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in the
// Software without restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "GraphView.h"
#import <GLKit/GLKMath.h> // for the awsome matrix math

@interface GraphView (/*Private*/)
 
@property (assign) CGPoint previousPoint;



@end

@implementation GraphView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

#pragma mark - Scene creation

- (void)commonInit
{
    
    self.shouldRotate = YES;
    // ==== SCENE CREATION ==== //
    self.showsStatistics = YES;
    self.backgroundColor = [UIColor blackColor];
    // An empty scene
    SCNScene *scene = [SCNScene scene];
    self.scene = scene;
    
	// A camera
    // --------
    // The camera is moved back and up from the center of the scene
    // and then rotated so that it looks down to the center
	SCNNode *cameraNode = [SCNNode node];
	cameraNode.camera = [SCNCamera camera];
	cameraNode.position = SCNVector3Make(0, 0, 30);
    // To make rotating using the track pad feel more natural the camera is not
    // rotated, instead the geometry starts off with a slight tilt
    
    [scene.rootNode addChildNode:cameraNode];
    self.allowsCameraControl = YES;
    
    
    // Two soft directional lights
    // ---------------------------
    // To create a nice lighting effect that both lights the surface
    // and causes it to be shaded on the areas facing away from the
    // camera two directional lights are used: one facing down and
    // the other having the default (down along z tilt).
    // Since the object is lit by both lights they are not as strong.
    SCNLight *directionDownLight = [SCNLight light];
    directionDownLight.type = SCNLightTypeDirectional;
    directionDownLight.color = [UIColor colorWithWhite:0.5 alpha:1.0];
	SCNNode *directionDownLightNode = [SCNNode node];
	directionDownLightNode.light = directionDownLight;
    
    directionDownLightNode.transform = SCNMatrix4Rotate(directionDownLightNode.transform,
                                                           -M_PI_2, 1, 0, 0);
    
    
    SCNLight *directionForwardLight = [SCNLight light];
    directionForwardLight.type = SCNLightTypeDirectional;
    directionForwardLight.color = [UIColor colorWithWhite:0.5 alpha:1.0];
	SCNNode *directionForwardLightNode = [SCNNode node];
	directionForwardLightNode.light = directionForwardLight;

    // The two lights are added to a node with a slight tilt to give
    // the illusion that the camera is tilted, not the geometry.
    SCNNode *lightNode = [SCNNode node];
    
    lightNode.transform = SCNMatrix4Rotate(lightNode.transform,
                                              M_PI/7.0,
                                              1, 0, 0);
    
    [lightNode addChildNode:directionDownLightNode];
    [lightNode addChildNode:directionForwardLightNode];
    [scene.rootNode addChildNode:lightNode];
    
    
    SCNNode *cameraNode2 = [SCNNode node];
    cameraNode2.light = [SCNLight new];
    cameraNode2.light.type = SCNLightTypeProbe;
    cameraNode2.position = SCNVector3Make(0, 0, -30);
    
    [scene.rootNode addChildNode:cameraNode2];

}

- (void) setGeometry:(SCNGeometry *)sine {
    SCNNode *sineNode = [SCNNode nodeWithGeometry:sine];
    self.graphNode = sineNode;
    _geometry = sine;
}

- (void) setGraphNode:(SCNNode *)graphNode {
    if(_graphNode != nil) {
        [_graphNode removeFromParentNode];
    }
    graphNode.name = @"sine";
    graphNode.position = SCNVector3Make(0, 0, 0);
    if (self.shouldRotate) {
        SCNVector3  min = SCNVector3Zero;
        SCNVector3  max = SCNVector3Zero;
        [graphNode getBoundingBoxMin:&min max:&max];
        CGFloat x = fabsf(max.x-min.x);
        CGFloat y = fabsf(max.y-min.y);
        CGFloat z = fabsf(max.z-min.z);
        if (x > y && x > z) {
            [graphNode runAction:[SCNAction repeatActionForever:[SCNAction rotateByX:1 y:0 z:0 duration:1]]];
        }
        else if (y > x && y > z) {
            [graphNode runAction:[SCNAction repeatActionForever:[SCNAction rotateByX:0 y:1 z:0 duration:1]]];
        }
        else if (z > x && z > y) {
            [graphNode runAction:[SCNAction repeatActionForever:[SCNAction rotateByX:0 y:0 z:1 duration:1]]];
        }

    }
    
    graphNode.light = [SCNLight new];
    
    [self.scene.rootNode addChildNode:graphNode];
    _graphNode = graphNode;
}

@end
