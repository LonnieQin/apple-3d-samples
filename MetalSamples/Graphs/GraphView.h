//
//  GraphView.h
//  DRMeshGeometry
//
//  Created by David Rönnqvist on 5/26/13.
//

#import <SceneKit/SceneKit.h>

@interface GraphView : SCNView
@property (nonatomic,strong) SCNGeometry * geometry;
@property (nonatomic) SCNNode *graphNode;
@property (nonatomic,assign) BOOL shouldRotate;
@end
