//
//  Curves.swift
//  ThreeDSamples
//
//  Created by Apple on 2017/3/8.
//  Copyright © 2017年 Apple. All rights reserved.
//

import Foundation
func CurvesSection()->Section {
    let section3 = Section(title:"Curves")
    section3.addRow(title: "Mobius") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(0, CGFloat(2*M_PI))
        builder.rangeTwo = DRMeshRangeMake(-1, 1)
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        var r:CGFloat = 1
        var R:CGFloat = 4
        let moebius = builder.geometry(xFunction: { (u,v) -> CGFloat in
            return (1+v/2*cos(u/2))*cos(u)
        }, yFunction: { (u, v) -> CGFloat in
            return (1+v/2*cos(u/2))*sin(u)
        }, zFunction: { (u, v) -> CGFloat in
            return v/2*sin(u/2)
        })
        moebius?.materials.first?.diffuse.contents = UIImage(named: "moebius")
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: moebius!), animated: true)
    }
    section3.addRow(title: "Klein") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(0, CGFloat(2*M_PI))
        builder.rangeTwo = DRMeshRangeMake(0, CGFloat(2*M_PI))
        builder.textureRepeatCounts = DRMeshCountMake(25, 4)
        let kleinBottle = builder.geometry(xFunction: { u,v in
            let val0 = sqrt(2)+cos(v)
            let val1 = cos(u/2.0)*val0
            let val2 = sin(u/2.0)*sin(v)*cos(v)
            return cos(u)*(val1+val2)
        }, yFunction: { u,v in
            let val0 = sqrt(2)+cos(v)
            let val1 = cos(u/2.0)*val0
            let val2 = sin(u/2.0)*sin(v)*cos(v)
            return sin(u)*(val1+val2)
        }, zFunction: { u,v in
            let val0 = sqrt(2)+cos(v)
            let val1 = cos(0.5*u)*sin(v)*cos(v)
            return -sin(u/2.0)*val0+val1
        })
        kleinBottle?.materials.first?.diffuse.contents = UIImage(named:"moebius")
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: kleinBottle!), animated: true)
    }
    //z = x^2+y^2
    //ucos(v)i+sin(v)j+u*uj
    section3.addRow(title: "Paraboid") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(0, 4)
        builder.rangeTwo = DRMeshRangeMake(0, 2*CGFloat(M_PI))
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        let paraboid = builder.geometry(xFunction: { u, v in u * cos(v)},
                                        yFunction: { u, v in u * sin(v)},
                                        zFunction: { u, v in u * u})
        paraboid?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: paraboid!), animated: true)
    }
    //椭圆抛物面
    //z = x^2/a^2+y^2/b^2
    //(ucos(v)/sqrt(a))i+(usin(v)/sqrt(b))j+u^2k
    section3.addRow(title: "Elliptic Paraboloid") { (v) in
        let builder = DRMeshGeometryBuilder()
        let a:CGFloat = 2
        let b:CGFloat = 3
        let R:CGFloat = 4
        builder.rangeOne = DRMeshRangeMake(0, R)
        builder.rangeTwo = DRMeshRangeMake(0, 2*CGFloat(M_PI))
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        let geometry = builder.geometry(xFunction:{ u, v in a * u * cos(v)},
                                        yFunction:{ u, v in b * u * sin(v)},
                                        zFunction:{ u, v in u * u})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    //双曲抛物面
    //z = x^2/a^2-y^2/b^2
    //(a())i+(usin(v)/sqrt(b))j+u^2k
    section3.addRow(title: "Hyperbolic paraboloid") { (v) in
        let builder = DRMeshGeometryBuilder()
        let a:CGFloat = 2
        let b:CGFloat = 3
        builder.rangeOne = DRMeshRangeMake(0, 4)
        builder.rangeTwo = DRMeshRangeMake(0, 2*CGFloat(M_PI))
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        let geometry = builder.geometry(xFunction: { u, v in u * cos(v)/sqrt(a)},
                                        yFunction: { u, v in u * sin(v)/sqrt(b)},
                                        zFunction: { u, v in u * u})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    section3.addRow(title: "Hyperboloid") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(0, 10)
        builder.rangeTwo = DRMeshRangeMake(0, 10)
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        let a:CGFloat = 4
        let b:CGFloat = 5
        let geometry = builder.geometry(xFunction: { u, v in a * (u + v)},
                                        yFunction: { u, v in b * (u - v)},
                                        zFunction: { u, v in 2 * u * v})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    section3.addRow(title: "Parabolic cylinder") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(-10, 10)
        builder.rangeTwo = DRMeshRangeMake(-10, 10)
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        let p:CGFloat = 5.0
        let geometry = builder.geometry(xFunction: { u, v in 2 * p * u * u },
                                        yFunction: { u, v in 2 * p * u},
                                        zFunction: { u, v in v})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    section3.addRow(title: "Elliptic cylinder") { (v) in
        let builder = DRMeshGeometryBuilder()
        
        builder.rangeOne = DRMeshRangeMake(0, 2*CGFloat(M_PI))
        builder.rangeTwo = DRMeshRangeMake(0, 10)
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        let a:CGFloat = 5
        let b:CGFloat = 2
        let geometry = builder.geometry(xFunction: { u, v in a*cos(u)},
                                        yFunction: { u, v in b*sin(u)},
                                        zFunction: { u, v in v})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    
    
    section3.addRow(title: "Cylndrical surface") { (v) in
        let builder = DRMeshGeometryBuilder()
        
        builder.rangeOne = DRMeshRangeMake(0, 2*CGFloat(M_PI))
        builder.rangeTwo = DRMeshRangeMake(0, 10)
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        let a:CGFloat = 5
        let geometry = builder.geometry(xFunction: { u, v in a*cos(u)},
                                        yFunction: { u, v in a*sin(u)},
                                        zFunction: { u, v in v})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    section3.addRow(title: "Circular cone") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(0, 2*CGFloat(M_PI))
        builder.rangeTwo = DRMeshRangeMake(-10, 10)
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        let geometry = builder.geometry(xFunction: { u, v in v*cos(u)},
                                        yFunction: { u, v in v*sin(u)},
                                        zFunction: { u, v in v})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    
    section3.addRow(title: "Elliptic cone") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(-10, 10)
        builder.rangeTwo = DRMeshRangeMake(0, 2*CGFloat(M_PI))
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        
        let a:CGFloat = 4
        let b:CGFloat = 5
        let c:CGFloat = 6
        let geometry = builder.geometry(xFunction: { u, v in a * u * cos(v)},
                                        yFunction: { u, v in b * u * sin(v)},
                                        zFunction: { u, v in c * u})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    
    section3.addRow(title: "Spherical surface") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(0, PI)
        builder.rangeTwo = DRMeshRangeMake(0, 2*PI)
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        
        let a:CGFloat = 4
        let geometry = builder.geometry(xFunction: { u, v in a * sin(u) * cos(v)},
                                        yFunction: { u, v in a * sin(u) * sin(v)},
                                        zFunction: { u, v in a * cos(u)})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    
    
    section3.addRow(title: "Ellipsoid") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(0, PI)
        builder.rangeTwo = DRMeshRangeMake(0, 2*PI)
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        
        let a:CGFloat = 4
        let b:CGFloat = 6
        let c:CGFloat = 6
        let geometry = builder.geometry(xFunction: { u, v in a * sin(u) * cos(v)},
                                        yFunction: { u, v in b * sin(u) * sin(v)},
                                        zFunction: { u, v in c * cos(u)})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    
    section3.addRow(title: "Torus") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(0, 2*PI)
        builder.rangeTwo = DRMeshRangeMake(0, 2*PI)
        builder.textureRepeatCounts = DRMeshCountMake(4, 4)
        
        let r:CGFloat = 1
        let R:CGFloat = 4
        let geometry = builder.geometry(xFunction: { u, v in cos(u) * cos(v) + R * cos(u)},
                                        yFunction: { u, v in sin(u) * cos(v) + R * sin(u)},
                                        zFunction: { u, v in r * sin(v)})
        geometry?.materials.first?.diffuse.contents = TextTure
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!), animated: true)
    }
    section3.addRow(title: "Cardioid") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(0, 2*PI)
        builder.rangeTwo = DRMeshRangeMake(0, 2*PI)
        let a:CGFloat = 5
        let geometry = builder.geometry(xFunction: { theta,t in
            return a * (2 * cos(theta) - cos(2 * theta)) * sin(0.5*t)
        },
                                                  yFunction: { theta,t in
                                                    return a * (2 * sin(theta) - sin(2 * theta)) * sin(0.5*t)
        },
                                                  zFunction: { theta,t in
                                                    return t
        })
        
        geometry?.materials.first?.multiply.contents = UIColor.red
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!,options:[.RotateKey:false]), animated: true)
    }
    
    section3.addRow(title: "Helicoid") { (v) in
        let builder = DRMeshGeometryBuilder()
        builder.rangeOne = DRMeshRangeMake(0, 10*PI)
        builder.rangeTwo = DRMeshRangeMake(0, 4)
        let a:CGFloat = 0.5
        let b:CGFloat = 0.05
        let geometry = builder.geometry(xFunction: { theta,t in (a+b*theta) * cos(theta)},
                                        yFunction: { theta,t in a+b*theta * sin(theta)},
                                        zFunction: { theta,t in t})
        
        geometry?.materials.first?.multiply.contents = UIColor.red
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry!,options:[.RotateKey:false]), animated: true)
    }
    section3.addRow(title: "Mountain") { (v) in
        let geometry = MountainGeometryBuilder().instance()
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geometry), animated: true)
    }
    return section3
}
