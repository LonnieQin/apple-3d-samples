//
//  Lines.swift
//  ThreeDSamples
//
//  Created by lonnie on 2017/3/7.
//  Copyright © 2017年 Apple. All rights reserved.
//

import Foundation

func CreateLine(radius R:CGFloat = 5,time T:CGFloat = 1,xFunction:Function,yFunction:Function,zFunction:Function,steps:UInt = 100)->SCNGeometry {
    let builder = DRMeshGeometryBuilder()
    //时间 theta
    builder.rangeOne = DRMeshRangeMake(0, 2*PI)
    //角度 t
    builder.rangeTwo = DRMeshRangeMake(0, T)
    builder.stepsPerAxisCounts = .init(one: 100, two: steps)
    var uvs:[String:(SCNVector3,SCNVector3)] = [:]
    let xDiff = xFunction.differential()
    let yDiff = yFunction.differential()
    let zDiff = zFunction.differential()
    func normalizes(_ t:CGFloat)->(SCNVector3,SCNVector3) {
        if uvs[t.description] != nil {return uvs[t.description]!}
        //圆所在平面的向量
        let a = CGFloat(xDiff[Double(t)])
        let b = CGFloat(yDiff[Double(t)])
        let norm = SCNVector3(x:Float(a),y:Float(b),z:Float(Float(zDiff[Double(t)])))
        //求出平面上一个向量
        var u = SCNVector3(x:Float(b),y:Float(-a),z:0)
        //求出和n,u都正交的向量v
        var v = crossProduct(a:norm,b:u)
        //U和V的单位向量
        u = normalize(u)
        v = normalize(v)
        uvs[t.description] = (u,v)
        return (u,v)
    }
    
    let geo = builder.geometry(xFunction: { theta,t in
        let (u,v) = normalizes(t)
        return CGFloat(xFunction[Double(t)])  + R * (CGFloat(u.x) * cos(theta) + CGFloat(v.x) * sin(theta))
    },
                               yFunction: { theta,t in
                                let (u,v) = normalizes(t)
                                return CGFloat(yFunction[Double(t)]) + R * (CGFloat(u.y) * cos(theta) + CGFloat(v.y) * sin(theta))
    },
                               zFunction: { theta,t in
                                let (u,v) = normalizes(t)
                                return CGFloat(zFunction[Double(t)]) + R * (CGFloat(u.z) * cos(theta) + CGFloat(v.z) * sin(theta))
    })
    
    return geo!
}

func LinesSection()->Section{
    let section = Section(title: "Space Curve")
    section.addRow(title: "r(t)=5*cos(t)i+5*sin(t)j+20*tk;") { (v) in
        let xFuntion = 5*cos(t)
        let yFuntion = 5*sin(t)
        let zFuntion = 20*t
        print(xFuntion.differential(),yFuntion.differential(),zFuntion.differential())
        let geo = CreateLine(radius:0.5,time:10,xFunction:xFuntion,yFunction:yFuntion,zFunction:zFuntion)
        geo.materials.first?.diffuse.contents = UIColor.red
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geo,options:[.RotateKey:false]), animated: true)
    }
    /*
    section.addRow(title: "(r(t)=sin(3t)cos(t)i+sin(3t)sin(t)j+tk)") { (v) in
        let xFuntion = sin(3*t)*cos(t)
        let yFuntion = sin(3*t)*sin(t)
        let zFuntion = t
        print(xFuntion.differential(),yFuntion.differential(),zFuntion.differential())
        let geo = CreateLine(radius:0.1,time:100,xFunction:xFuntion,yFunction:yFuntion,zFunction:zFuntion,steps: 1000)
        geo.materials.first?.diffuse.contents = UIColor.red
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geo,options:[.RotateKey:false]), animated: true)
    }
    */
    /*
    section.addRow(title: "(r(t)=cos(t)i+sin(t)j+sin(2t)k)") { (v) in
        let xFuntion = cos(t)
        let yFuntion = sin(t)
        let zFuntion = sin(2*t)
        print(xFuntion.differential(),yFuntion.differential(),zFuntion.differential())
        let geo = CreateLine(radius:5,time:100,xFunction:xFuntion,yFunction:yFuntion,zFunction:zFuntion)
        geo.materials.first?.diffuse.contents = UIColor.red
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geo,options:[.RotateKey:false]), animated: true)
    }
    */
    section.addRow(title: "r(t)=cos(t)i+sin(t)j+0.3tk") { (v) in
        let xFuntion = cos(5*t)
        let yFuntion = sin(5*t)
        let zFuntion = t
        print(xFuntion.differential(),yFuntion.differential(),zFuntion.differential())
        let geo = CreateLine(radius:0.1,time:10,xFunction:xFuntion,yFunction:yFuntion,zFunction:zFuntion,steps:100)
        geo.materials.first?.diffuse.contents = UIColor.red
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geo,options:[.RotateKey:false]), animated: true)
    }
    /*
    section.addRow(title: "(r(t)=cos(t)i+sin(t)j+t^2k)") { (v) in
        let xFuntion = 3*cos(t)
        let yFuntion = 3*sin(t)
        let zFuntion = 0.3*PowerFunction(baseFunction: t, index: 2)
        print(xFuntion.differential(),yFuntion.differential(),zFuntion.differential())
        let geo = CreateLine(radius:1,time:10,xFunction:xFuntion,yFunction:yFuntion,zFunction:zFuntion,steps:100)
        geo.materials.first?.diffuse.contents = UIColor.red
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geo,options:[.RotateKey:false]), animated: true)
    }
    */

    section.addRow(title: "Line") { (v) in
        let spatialLine = SpatialLine(point: .init(2, 2, 3), vector: .init(3, 2, 5))
        let fun = spatialLine.parameterFunxtion()
        let xFuntion = fun.xFunction
        let yFuntion = fun.yFunction
        let zFuntion = fun.zFunction
        print(xFuntion.differential(),yFuntion.differential(),zFuntion.differential())
        let geo = CreateLine(radius:1,time:50,xFunction:xFuntion,yFunction:yFuntion,zFunction:zFuntion)
        geo.materials.first?.diffuse.contents = UIColor.red
        v.navigationController?.pushViewController(GraphViewController.instance(geometry: geo,options:[.RotateKey:false]), animated: true)
        
    }
    return section
}
