//
//  Scenes.swift
//  ThreeDSamples
//
//  Created by Apple on 2017/3/8.
//  Copyright © 2017年 Apple. All rights reserved.
//

import Foundation
func SceneSection()->Section {
    let section2 = Section(title: "3D Scenes")
    section2.addRow(title: "Adventure game") { (c) in
        DispatchQueue.global().async {
            let badger = BadgerViewController.instance()
            DispatchQueue.main.async {
                c.navigationController?.pushViewController(badger, animated: true)
            }
        }
    }
    section2.addRow(title: "Car Game") { (c) in
        DispatchQueue.global().async {
            let gameController = CarGameViewController.instance()
            DispatchQueue.main.async {
                c.navigationController?.pushViewController(gameController, animated: true)
            }
        }
    }
    section2.addRow(title: "SceneKit Techniques") { (controller) in
        let c = AAPLPresentationViewController(dictionary:NSDictionary(contentsOfFile: Bundle.main.path(forResource: "Scene Kit Presentation", ofType: "plist")!) as! [AnyHashable : Any]!)!
        controller.navigationController?.present(c, animated: true, completion: nil)
    }
    return section2
}
