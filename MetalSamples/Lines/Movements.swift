//
//  Movements.swift
//  ThreeDSamples
//
//  Created by Apple on 2017/3/8.
//  Copyright © 2017年 Apple. All rights reserved.
//

import Foundation
import SceneKit

func ShowSphereNode(_ vc:UIViewController,block:(SCNNode)->Void) {
    let ps = SCNParticleSystem(named:"fire",inDirectory:nil)
    let node = SCNNode(geometry:SCNSphere(radius:1))
    node.light = SCNLight()
    node.light?.type = .directional
    node.light?.zFar = 200
    //node.scale = .init(0.5, 0.5, 0.5)
    node.addParticleSystem(ps!)
    //node.geometry?.materials.first?.diffuse.contents = "earth-diffuse-mini.jpg"
    vc.navigationController?.pushViewController(GraphViewController.instance(node: node,options:[.RotateKey:false]), animated: true)
    block(node)
}
func MovementSection()->Section {
    let section = Section(title:"Motions")
    section.addRow(title: "Circular Motion") {
        ShowSphereNode($0, block: {$0.runAction(.moveBy(function:Parameter3DFunction(x: 5*sin(t), y: 5*cos(t),z:Number(3))))})
    }
    section.addRow(title: "Simple Harmonic Motion") {
        ShowSphereNode($0, block: {$0.runAction(.moveBy(function:Parameter3DFunction(x: t, y:5*sin(t),z:Number(3))))})
    }
    section.addRow(title: "Projectile Motion") {
        let projectTile = Projectile(v0: 5, angle: 0, position0: CGPoint(x: 0, y: 10))
        let function = projectTile.function()
        ShowSphereNode($0, block: {$0.runAction(.moveBy(function:Parameter3DFunction(x: function.xFunction, y:function.yFunction,z:Number(-50))))})
    }
    section.addRow(title: "Heart Motion") {
        ShowSphereNode($0, block: {$0.runAction(.moveBy(function:Parameter3DFunction(polarFunction:PolerFunction(5-5*cos(t)),z:Number(10))))})
    }
    section.addRow(title: "Helical Motion") {
        ShowSphereNode($0, block: {$0.runAction(.moveBy(function:Parameter3DFunction(polarFunction:PolerFunction(t),z:Number(-50))))})
    }
    return section
}
