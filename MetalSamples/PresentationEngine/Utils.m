/*
 Copyright (C) 2014-2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 This file contains some utilities such as titled box, loading DAE files, loading images etc...
 */

#import <GLKit/GLKMath.h>
#import "Utils.h"
UIImage * convertBitmapRGBA8ToUIImage(unsigned char * buffer,int width,int height) {
    
    
    size_t bufferLength = width * height * 4;
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer, bufferLength, NULL);
    size_t bitsPerComponent = 8;
    size_t bitsPerPixel = 32;
    size_t bytesPerRow = 4 * width;
    
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    if(colorSpaceRef == NULL) {
        NSLog(@"Error allocating color space");
        CGDataProviderRelease(provider);
        return nil;
    }
    
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault | kCGImageAlphaPremultipliedLast;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    
    CGImageRef iref = CGImageCreate(width,
                                    height,
                                    bitsPerComponent,
                                    bitsPerPixel,
                                    bytesPerRow,
                                    colorSpaceRef,
                                    bitmapInfo,
                                    provider,    // data provider
                                    NULL,        // decode
                                    YES,            // should interpolate
                                    renderingIntent);
    
    uint32_t* pixels = (uint32_t*)malloc(bufferLength);
    
    if(pixels == NULL) {
        NSLog(@"Error: Memory not allocated for bitmap");
        CGDataProviderRelease(provider);
        CGColorSpaceRelease(colorSpaceRef);
        CGImageRelease(iref);
        return nil;
    }
    
    CGContextRef context = CGBitmapContextCreate(pixels,
                                                 width,
                                                 height,
                                                 bitsPerComponent,
                                                 bytesPerRow,
                                                 colorSpaceRef,
                                                 bitmapInfo);
    
    if(context == NULL) {
        NSLog(@"Error context not created");
        free(pixels);
    }
    
    UIImage *image = nil;
    if(context) {
        
        CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, width, height), iref);
        
        CGImageRef imageRef = CGBitmapContextCreateImage(context);
        
        // Support both iPad 3.2 and iPhone 4 Retina displays with the correct scale
        if([UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
            float scale = [[UIScreen mainScreen] scale];
            image = [UIImage imageWithCGImage:imageRef scale:scale orientation:UIImageOrientationUp];
        } else {
            image = [UIImage imageWithCGImage:imageRef];
        }
        
        CGImageRelease(imageRef);
        CGContextRelease(context);
    }
    
    CGColorSpaceRelease(colorSpaceRef);
    CGImageRelease(iref);
    CGDataProviderRelease(provider);
    
    if(pixels) {
        free(pixels);
    }
    return image;
}

CGContextRef newBitmapRGBA8ContextFromImage(CGImageRef image) {
    CGContextRef context = NULL;
    CGColorSpaceRef colorSpace;
    uint32_t *bitmapData;
    
    size_t bitsPerPixel = 32;
    size_t bitsPerComponent = 8;
    size_t bytesPerPixel = bitsPerPixel / bitsPerComponent;
    
    size_t width = CGImageGetWidth(image);
    size_t height = CGImageGetHeight(image);
    
    size_t bytesPerRow = width * bytesPerPixel;
    size_t bufferLength = bytesPerRow * height;
    
    colorSpace = CGColorSpaceCreateDeviceRGB();
    
    if(!colorSpace) {
        NSLog(@"Error allocating color space RGB\n");
        return NULL;
    }
    
    // Allocate memory for image data
    bitmapData = (uint32_t *)malloc(bufferLength);
    
    if(!bitmapData) {
        NSLog(@"Error allocating memory for bitmap\n");
        CGColorSpaceRelease(colorSpace);
        return NULL;
    }
    
    //Create bitmap context
    
    context = CGBitmapContextCreate(bitmapData,
                                    width,
                                    height,
                                    bitsPerComponent,
                                    bytesPerRow,
                                    colorSpace,
                                    kCGImageAlphaPremultipliedLast);    // RGBA
    if(!context) {
        free(bitmapData);
        NSLog(@"Bitmap context not created");
    }
    
    CGColorSpaceRelease(colorSpace);
    
    return context;
}
unsigned char * convertUIImageToBitmapRGBA8(UIImage *  image) {
    
    CGImageRef imageRef = image.CGImage;
    
    // Create a bitmap context to draw the uiimage into
    CGContextRef context = newBitmapRGBA8ContextFromImage(imageRef);
    
    if(!context) {
        return NULL;
    }
    
    size_t width = CGImageGetWidth(imageRef);
    size_t height = CGImageGetHeight(imageRef);
    
    CGRect rect = CGRectMake(0, 0, width, height);
    
    // Draw image into the context to get the raw image data
    CGContextDrawImage(context, rect, imageRef);
    
    // Get a pointer to the data
    unsigned char *bitmapData = (unsigned char *)CGBitmapContextGetData(context);
    
    // Copy the data and release the memory (return memory allocated with new)
    size_t bytesPerRow = CGBitmapContextGetBytesPerRow(context);
    size_t bufferLength = bytesPerRow * height;
    
    unsigned char *newBitmap = NULL;
    
    if(bitmapData) {
        newBitmap = (unsigned char *)malloc(sizeof(unsigned char) * bytesPerRow * height);
        
        if(newBitmap) {    // Copy the data
            for(int i = 0; i < bufferLength; ++i) {
                newBitmap[i] = bitmapData[i];
            }
        }
        
        free(bitmapData);
        
    } else {
        NSLog(@"Error getting bitmap pixel data\n");
    }
    
    CGContextRelease(context);
    
    return newBitmap;
}





static void _markBuffer(BOOL *mark, NSInteger pos, int count, NSInteger dx)
{
    for(int i=0; i<count; i++, pos+=dx){
        mark[pos] = 1;
    }
}

static BOOL _sameColor(NSInteger pos, int count, BOOL white, NSInteger dx, unsigned char *data)
{
    for(NSInteger i=0, tdx=0; i<count; i++, tdx+=dx){
        if(data[(pos + tdx)*4 + 3] < 128) return NO;
        unsigned char c = data[(pos + tdx)*4];
        if(white && c < 128) return NO;
        if(!white && c > 128) return NO;
    }
    
    return YES;
}

static int _extend(NSInteger x, NSInteger y, NSInteger w, NSInteger h, unsigned char *data, BOOL isWhite, BOOL *mark)
{
    int c=1;
    BOOL shouldContinue = 1;
    
    do{
        if(x+c < w && y+c < h
           && _sameColor(x+c+y*w, c+1, isWhite, w, data)
           && _sameColor(x+(y+c)*w, c, isWhite, 1, data)){
            //mark
            _markBuffer(mark, x+c+y*w, c+1, w);
            _markBuffer(mark, x+(y+c)*w, c, 1);
            //next
            c++;
        }
        else{
            shouldContinue = 0;
        }
        
        
    }while(shouldContinue);
    
    return c;
}

@implementation SCNNode (AAPLAdditions)


+ (SCNNode *) nodeWithPixelatedImage:(UIImage *) image  pixelSize:(CGFloat) size
{
    
    NSInteger w = image.size.width;
    NSInteger h = image.size.height;
    unsigned char *data = convertUIImageToBitmapRGBA8(image);
    
    BOOL *mark = (BOOL*) calloc(1, w*h);
    
    SCNMaterial *white = [SCNMaterial material];
    SCNMaterial *black = [SCNMaterial material];
    black.diffuse.contents = [UIColor orangeColor];
    //black.reflective.contents = @"envmap.jpg";
    
    SCNNode *group = [SCNNode node];
    
    for(NSInteger y=0, index=0; y<h; y++){
        for(NSInteger x=0; x<w; x++, index++){
            if(mark[index]) continue;
            if(data[index*4+3] < 128) continue;
            
            bool isWhite = (data[index*4] > 128);
            
            int count = _extend(x, y, w, h, data, isWhite, mark);
            float blockSize = size * count;
            
            SCNBox *box = [SCNBox boxWithWidth:blockSize height:blockSize length:5*size chamferRadius:blockSize * 0.0];
            box.firstMaterial = isWhite ? white : black;
            
            SCNNode *node = [SCNNode node];
            node.position = SCNVector3Make((x-(w/2))*size + (count-1)*size*0.5, (h-y)*size - (count-1)*size*0.5, 0);
            node.geometry = box;
            
            [group addChildNode:node];
        }
    }
    
    return group;
}

- (SCNNode *)asc_addChildNodeNamed:(NSString *)name fromSceneNamed:(NSString *)path withScale:(CGFloat)scale {
    // Load the scene from the specified file
    SCNScene *scene = [SCNScene sceneNamed:path inDirectory:nil options:nil];
    
    // Retrieve the root node
    SCNNode *node = scene.rootNode;
    
    // Search for the node named "name"
    if (name) {
        node = [node childNodeWithName:name recursively:YES];
    }
    else {
        // Take the first child if no name is passed
        node = node.childNodes[0];
    }
    
    if (scale != 0) {
        // Rescale based on the current bounding box and the desired scale
        // Align the node to 0 on the Y axis
        SCNVector3 min, max;
        [node getBoundingBoxMin:&min max:&max];
        
        GLKVector3 mid = GLKVector3Add(SCNVector3ToGLKVector3(min), SCNVector3ToGLKVector3(max));
        mid = GLKVector3MultiplyScalar(mid, 0.5);
        mid.y = min.y; // Align on bottom
        
        GLKVector3 size = GLKVector3Subtract(SCNVector3ToGLKVector3(max), SCNVector3ToGLKVector3(min));
        CGFloat maxSize = MAX(MAX(size.x, size.y), size.z);
        
        scale = scale / maxSize;
        mid = GLKVector3MultiplyScalar(mid, scale);
        mid = GLKVector3Negate(mid);
        
        node.scale = SCNVector3Make(scale, scale, scale);
        node.position = SCNVector3FromGLKVector3(mid);
    }
    
    // Add to the container passed in argument
    [self addChildNode:node];
    
    return node;
}

+ (instancetype)asc_boxNodeWithTitle:(NSString *)title frame:(CGRect)frame color:(UIColor *)color cornerRadius:(CGFloat)cornerRadius centered:(BOOL)centered {
    static NSDictionary *titleAttributes = nil;
    static NSDictionary *centeredTitleAttributes = nil;
    
    // create and extrude a bezier path to build the box
   // NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:frame xRadius:cornerRadius yRadius:cornerRadius];
    UIBezierPath * path = [UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:cornerRadius];
    path.flatness = 0.05;
    
    SCNShape *shape = [SCNShape shapeWithPath:path extrusionDepth:20];
    shape.chamferRadius = 0.0;
    
    SCNNode *node = [SCNNode node];
    node.geometry = shape;
    
    // create an image and fill with the color and text
    CGSize textureSize;
    textureSize.width = ceilf(frame.size.width * 1.5);
    textureSize.height = ceilf(frame.size.height * 1.5);
    
    UIImage *texture = [UIImage new];
    
    CGRect drawFrame = CGRectMake(0, 0, textureSize.width, textureSize.height);
    
    CGFloat hue, saturation, brightness, alpha;
    [color getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    //[[color colorUsingColorSpace:[ deviceRGBColorSpace]] getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    UIColor *lightColor = [UIColor colorWithHue:hue saturation:saturation-0.2 brightness:brightness+0.3 alpha:alpha];
    UIGraphicsBeginImageContext(drawFrame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    [lightColor set];
    CGContextFillRect(context, drawFrame);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
        //NSRectFill(drawFrame);
    
    UIBezierPath *fillpath = nil;
    
    if (cornerRadius == 0 && centered == NO) {
        //special case for the "labs" slide
        
        fillpath = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(drawFrame, 0, -2) cornerRadius:cornerRadius];
    }
    else {
        fillpath = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(drawFrame, 3, 3) cornerRadius:cornerRadius];
    }
    
    [color set];
    [fillpath fill];
    
    // draw the title if any
    if (title) {
        if (titleAttributes == nil) {
            NSMutableParagraphStyle *paraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
            [paraphStyle setAlignment:NSTextAlignmentLeft];
            [paraphStyle setMinimumLineHeight:38];
            [paraphStyle setMaximumLineHeight:38];
            
            UIFont *font = [UIFont fontWithName:@"Myriad Set" size:34] ?: [UIFont fontWithName:@"Avenir Medium" size:34];
            
            NSShadow *shadow = [[NSShadow alloc] init];
            
            [shadow setShadowOffset:CGSizeMake(0, -2)];
            [shadow setShadowBlurRadius:4];
            [shadow setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
            
            titleAttributes = @{ NSFontAttributeName            : font,
                                 NSForegroundColorAttributeName : [UIColor whiteColor],
                                 NSShadowAttributeName          : shadow,
                                 NSParagraphStyleAttributeName  : paraphStyle };
            
            
            NSMutableParagraphStyle *centeredParaphStyle = [paraphStyle mutableCopy];
            [centeredParaphStyle setAlignment:NSTextAlignmentCenter];
            
            centeredTitleAttributes = @{ NSFontAttributeName            : font,
                                         NSForegroundColorAttributeName : [UIColor whiteColor],
                                         NSShadowAttributeName          : shadow,
                                         NSParagraphStyleAttributeName  : centeredParaphStyle };
        }
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:title attributes:centered ? centeredTitleAttributes : titleAttributes];
        CGSize textSize = [attrString size];
        //check if we need two lines to draw the text
        BOOL twoLines = [title rangeOfString:@"\n"].length > 0;
        if (!twoLines) {
            twoLines = textSize.width > frame.size.width && [title rangeOfString:@" "].length > 0;
        }
        
        //if so, we need to adjust the size to center vertically
        if (twoLines) {
            textSize.height += 38;
        }
        
        if (!centered)
            drawFrame = CGRectInset(drawFrame, 15, 0);
        
        //center vertically
        float dy = (drawFrame.size.height - textSize.height) * 0.5;
        drawFrame.size.height -= dy;
        [attrString drawInRect:drawFrame];
    }
    
    
    //set the created image as the diffuse texture of our 3D box
    SCNMaterial *front = [SCNMaterial material];
    front.diffuse.contents = texture;
    front.locksAmbientWithDiffuse = YES;
    
    //use a lighter color for the chamfer and sides
    SCNMaterial *sides = [SCNMaterial material];
    sides.diffuse.contents = lightColor;
    node.geometry.materials = @[front, sides, sides, sides, sides];
    
    return node;
}

+ (instancetype)asc_planeNodeWithImage:(UIImage *)image size:(CGFloat)size isLit:(BOOL)isLit {
    SCNNode *node = [SCNNode node];
    
    float factor = size / (MAX(image.size.width, image.size.height));
    
    node.geometry = [SCNPlane planeWithWidth:image.size.width*factor height:image.size.height*factor];
    node.geometry.firstMaterial.diffuse.contents = image;
    
    //if we don't want the image to be lit, set the lighting model to "constant"
    if (!isLit)
        node.geometry.firstMaterial.lightingModelName = SCNLightingModelConstant;
    
    return node;
}

+ (instancetype)asc_planeNodeWithImageNamed:(NSString *)imageName size:(CGFloat)size isLit:(BOOL)isLit {
    return [self asc_planeNodeWithImage:[UIImage imageNamed:imageName] size:size isLit:isLit];
}

+ (instancetype)asc_labelNodeWithString:(NSString *)string size:(AAPLLabelSize)size isLit:(BOOL)isLit {
    SCNNode *node = [SCNNode node];
    
    SCNText *text = [SCNText textWithString:string extrusionDepth:0];
    node.geometry = text;
    node.scale = SCNVector3Make(0.01 * size, 0.01 * size, 0.01 * size);
    text.flatness = 0.4;
    
    // Use Myriad it's if available, otherwise Avenir
    if(size == AAPLLabelSizeLarge)
        text.font = [UIFont fontWithName:@"Myriad Set Bold" size:50] ?: [UIFont fontWithName:@"Avenir bold" size:50];
    else
        text.font = [UIFont fontWithName:@"Myriad Set" size:50] ?: [UIFont fontWithName:@"Avenir Medium" size:50];
    
    if (!isLit) {
        text.firstMaterial.lightingModelName = SCNLightingModelConstant;
    }
    
    return node;
}

+ (instancetype)asc_gaugeNodeWithTitle:(NSString *)title progressNode:(SCNNode * __strong *)progressNode {
    SCNNode *gaugeGroup = [SCNNode node];
    
    [SCNTransaction begin];
    [SCNTransaction setAnimationDuration:0];
    {
        SCNNode *gauge = [SCNNode node];
        gauge.geometry = [SCNCapsule capsuleWithCapRadius:0.4 height:8];
        gauge.geometry.firstMaterial.lightingModelName = SCNLightingModelConstant;
        gauge.rotation = SCNVector4Make(0, 0, 1, M_PI_2);
        gauge.geometry.firstMaterial.diffuse.contents = [UIColor whiteColor];
        gauge.geometry.firstMaterial.cullMode = SCNCullFront;
        
        SCNNode *gaugeValue = [SCNNode node];
        gaugeValue.geometry = [SCNCapsule capsuleWithCapRadius:0.3 height:7.8];
        gaugeValue.pivot = SCNMatrix4MakeTranslation(0, 3.8, 0);
        gaugeValue.position = SCNVector3Make(0, 3.8, 0);
        gaugeValue.scale = SCNVector3Make(1, 0.01, 1);
        gaugeValue.opacity = 0.0;
        gaugeValue.geometry.firstMaterial.diffuse.contents = [UIColor colorWithRed:0 green:1 blue:0 alpha:1];
        gaugeValue.geometry.firstMaterial.lightingModelName = SCNLightingModelConstant;
        [gauge addChildNode:gaugeValue];
        
        if (progressNode) {
            *progressNode = gaugeValue;
        }
        
        SCNNode *titleNode = [SCNNode asc_labelNodeWithString:title  size:AAPLLabelSizeNormal isLit:NO];
        titleNode.position = SCNVector3Make(-8, -0.55, 0);
        
        [gaugeGroup addChildNode:titleNode];
        [gaugeGroup addChildNode:gauge];
    }
    [SCNTransaction commit];
    
    return gaugeGroup;
}

@end

@implementation UIBezierPath (AAPLAdditions)

+ (instancetype)asc_arrowBezierPathWithBaseSize:(CGSize)baseSize tipSize:(CGSize)tipSize hollow:(CGFloat)hollow twoSides:(BOOL)twoSides {
    UIBezierPath *arrow = [UIBezierPath bezierPath];
    
    float h[5];
    float w[4];
    
    w[0] = 0;
    w[1] = baseSize.width - tipSize.width - hollow;
    w[2] = baseSize.width - tipSize.width;
    w[3] = baseSize.width;
    
    h[0] = 0;
    h[1] = (tipSize.height - baseSize.height) * 0.5;
    h[2] = (tipSize.height) * 0.5;
    h[3] = (tipSize.height + baseSize.height) * 0.5;
    h[4] = tipSize.height;
    
    if (twoSides) {
        [arrow moveToPoint:CGPointMake(tipSize.width, h[1])];
        
        [arrow addLineToPoint:CGPointMake(tipSize.width + hollow, h[0])];
        [arrow addLineToPoint:CGPointMake(0, h[2])];
        [arrow addLineToPoint:CGPointMake(tipSize.width + hollow, h[4])];
        [arrow addLineToPoint:CGPointMake(tipSize.width, h[3])];
    }
    else {
        [arrow moveToPoint:CGPointMake(0, h[1])];
        [arrow addLineToPoint:CGPointMake(0, h[3])];
    }
    
    [arrow addLineToPoint:CGPointMake(w[2], h[3])];
    [arrow addLineToPoint:CGPointMake(w[1], h[4])];
    [arrow addLineToPoint:CGPointMake(w[3], h[2])];
    [arrow addLineToPoint:CGPointMake(w[1], h[0])];
    [arrow addLineToPoint:CGPointMake(w[2], h[1])];
    
    [arrow closePath];
    
    return arrow;
}



@end


@implementation SCNAction (AAPLAddition)

+ (SCNAction *) removeFromParentNodeOnMainThread:(SCNNode *) node
{
    return [SCNAction runBlock:^(SCNNode *owner){
        [owner removeFromParentNode];
    } queue:dispatch_get_main_queue()];
}

@end

