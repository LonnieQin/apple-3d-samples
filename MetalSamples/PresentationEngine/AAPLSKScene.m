//
//  AAPLSKScene.m
//  ThreeDSamples
//
//  Created by Apple on 2017/2/23.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "AAPLSKScene.h"

@implementation AAPLSKScene {
    SKLabelNode * left;
    SKLabelNode * right;
    SKLabelNode * exit;
}
- (instancetype) initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    left = [SKLabelNode new];
    left.name = @"left";
    left.text = @"⬅️";
    right = [SKLabelNode new];
    right.text = @"➡️";
    right.name = @"right";
    [self addChild:left];
    [self addChild:right];
    left.position = CGPointMake(45, 10);
    right.position = CGPointMake(size.width-45, 10);
    exit = [SKLabelNode new];
    exit.text = @"❎";
    exit.name = @"exit";
    exit.fontColor = [SKColor whiteColor];
    exit.position = CGPointMake(45, size.height-40);
    [self addChild:exit];
    return self;
}

@end
