/*
 Copyright (C) 2014-2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 AAPLView is a subclass of SCNView. The only thing it does is to force a resolution
 */

#import <SceneKit/SceneKit.h>
@interface AAPLView : SCNView
@property (nonatomic,copy) dispatch_block_t leftHandler;
@property (nonatomic,copy) dispatch_block_t rightHandler;
@property (nonatomic,copy) dispatch_block_t exitHandler;
@end

#define FORCE_RESOLUTION 0
#define RESOLUTION_X [UIScreen mainScreen].bounds.size.width
#define RESOLUTION_Y [UIScreen mainScreen].bounds.size.height

