//
//  TessellationViewController.swift
//  ThreeDSamples
//
//  Created by Apple on 2017/2/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit
import MetalKit
class TessellationViewController: UIViewController {
    @IBOutlet weak var mtkView: MTKView!

    @IBOutlet weak var edgeLabel: UILabel!
    @IBOutlet weak var insideLabel: UILabel!
    //var pipelineLine:AAPLTessellationPipeline?
    var pipelineLine:TessellationPipeline?
    override func viewDidLoad() {
        super.viewDidLoad()
        mtkView.isPaused = true
        mtkView.enableSetNeedsDisplay = true
        mtkView.sampleCount = 4
        //pipelineLine = AAPLTessellationPipeline(mtkView: mtkView)
        pipelineLine = TessellationPipeline(mtkView: mtkView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        mtkView.draw()
    }
    
    @IBAction func changeSegment(_ sender: UISegmentedControl) {
        pipelineLine!.patchType = (sender.selectedSegmentIndex == 0) ? .triangle : .quad
        mtkView.draw()
    }
    @IBAction func wireFrameDidChange(_ sender: UISwitch) {
        pipelineLine!.wireframe = sender.isOn
        mtkView.draw()
    }
    @IBAction func edgeSliderDidChange(_ sender: UISlider) {
        edgeLabel.text = "\(sender.value)"
        mtkView.draw()
    }
    @IBAction func insideSliderDidChange(_ sender: UISlider) {
        insideLabel.text = "\(sender.value)"
        mtkView.draw()
    }
}
