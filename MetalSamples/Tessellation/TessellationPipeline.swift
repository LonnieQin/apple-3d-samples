//
//  TessellationPipeline.swift
//  ThreeDSamples
//
//  Created by Apple on 2017/2/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit
import Metal
import MetalKit
class TessellationPipeline: NSObject,MTKViewDelegate  {
    var patchType:MTLPatchType = .triangle
    var wireframe:Bool = true
    var edgeFactor:Float = 2
    var insideFactor:Float = 2
    private var device:MTLDevice
    private var commandQueue:MTLCommandQueue
    private var library:MTLLibrary
    private var computePipelineStateTrangle:MTLComputePipelineState?
    private var computePipelineStateQuad:MTLComputePipelineState?
    private var renderPipelineStateTrangle:MTLRenderPipelineState?
    private var renderPipelineStateQuad:MTLRenderPipelineState?
    private var tessellationFactorsBuffer:MTLBuffer
    private var controlPointsBufferTrangle:MTLBuffer
    private var controlPointsBufferQuad:MTLBuffer
    
    init(mtkView:MTKView) {
        //Set up Metal
        device = MTLCreateSystemDefaultDevice()!
        mtkView.device = device
        commandQueue = device.makeCommandQueue()
        library = device.newDefaultLibrary()!
        
        //Set up Compute pipeline
        let kernelFunctionTrangle = library.makeFunction(name: "tessellation_kernel_triangle")!
        let kernelFunctionQuad = library.makeFunction(name: "tessellation_kernel_quad")!
        do {
            computePipelineStateTrangle = try device.makeComputePipelineState(function: kernelFunctionTrangle)
            computePipelineStateQuad = try device.makeComputePipelineState(function: kernelFunctionQuad)
        } catch  {
            print("Error")
        }
        
        //Set up RenderPipelines
        let vertexDescriptor = MTLVertexDescriptor()
        vertexDescriptor.attributes[0].format = .float4
        vertexDescriptor.attributes[0].offset = 0
        vertexDescriptor.attributes[0].bufferIndex = 0
        vertexDescriptor.layouts[0].stepFunction = .perPatchControlPoint
        vertexDescriptor.layouts[0].stepRate = 1
        vertexDescriptor.layouts[0].stride = 4 * MemoryLayout<Float>.size
        
        let renderPipelineDescriptor = MTLRenderPipelineDescriptor()
        renderPipelineDescriptor.vertexDescriptor = vertexDescriptor
        renderPipelineDescriptor.sampleCount = mtkView.sampleCount
        renderPipelineDescriptor.colorAttachments[0].pixelFormat = mtkView.colorPixelFormat
        renderPipelineDescriptor.fragmentFunction = library.makeFunction(name: "tessellation_fragment")
        
        //Configure common tessellation properties
        renderPipelineDescriptor.isTessellationFactorScaleEnabled = false
        renderPipelineDescriptor.tessellationFactorFormat = .half
        renderPipelineDescriptor.tessellationControlPointIndexType = .none
        renderPipelineDescriptor.tessellationFactorStepFunction = .constant
        renderPipelineDescriptor.tessellationOutputWindingOrder = .clockwise
        renderPipelineDescriptor.tessellationPartitionMode = .fractionalEven
        //IOS 16 mac 64
        renderPipelineDescriptor.maxTessellationFactor = 16
        
        
        do {
            //Create render pipeline for triangle-based tessellation
            renderPipelineDescriptor.vertexFunction = library.makeFunction(name: "tessellation_vertex_triangle")
            renderPipelineStateTrangle = try device.makeRenderPipelineState(descriptor: renderPipelineDescriptor)
            //Create render pipeline for triangle-based tessellation
            renderPipelineDescriptor.vertexFunction = library.makeFunction(name: "tessellation_vertex_quad")
            renderPipelineStateQuad = try device.makeRenderPipelineState(descriptor: renderPipelineDescriptor)
            
        } catch {
            
        }
        
        //Step Buffers
        tessellationFactorsBuffer = device.makeBuffer(length: 256, options: .storageModePrivate)
        tessellationFactorsBuffer.label = "Tessellation Factors"
        let controlPointsOption:MTLResourceOptions = .storageModeShared
 
        var controlPointsTriangle:[CFloat] = [-0.8,-0.8,0.0,1.0,//Lower left
                                             0.0,0.8,0.0,1.0,//Lower middle
                                             0.8,-0.8,0.0,1.0]//Lower right
        let length1 = controlPointsTriangle.count * MemoryLayout<CFloat>.size
        controlPointsBufferTrangle = device.makeBuffer(length: length1, options:controlPointsOption)
        let Ptr1 = controlPointsBufferTrangle.contents().bindMemory(to: CFloat.self, capacity: length1)
        Ptr1.assign(from: &controlPointsTriangle, count: controlPointsTriangle.count)
        controlPointsBufferTrangle.label = "Control Points Triangle"
        
        var controlPointsQuad:[CFloat] = [-0.8,0.8,0.0,1.0,//upper-left,
                                          0.8,0.8,0.0,1.0,//upper-right,
                                          0.8,-0.8,0.0,1.0,//lower-right,
                                          -0.8,-0.8,0.0,1.0]//lower-left
        let length2 = controlPointsQuad.count * MemoryLayout<CFloat>.size
        controlPointsBufferQuad = device.makeBuffer(length: length2, options:controlPointsOption)
        let Ptr2 = controlPointsBufferTrangle.contents().bindMemory(to: CFloat.self, capacity: length2)
        Ptr2.assign(from: &controlPointsQuad, count: controlPointsQuad.count)
        controlPointsBufferQuad.label = "Control Points Quad"
        super.init()
        mtkView.delegate = self
    }
    
    func computeTessellationFactors(commandBuffer:MTLCommandBuffer) {
        let computeCommandEncoder = commandBuffer.makeComputeCommandEncoder()
        computeCommandEncoder.label = "Compute Command Encoder"
        computeCommandEncoder.pushDebugGroup("Compute Tessellation Factors")
        if patchType == .triangle {
            computeCommandEncoder.setComputePipelineState(computePipelineStateTrangle!)
        } else {
            computeCommandEncoder.setComputePipelineState(computePipelineStateQuad!)
        }
        
        computeCommandEncoder.setBytes(&edgeFactor, length: MemoryLayout<Float>.size, at: 0)
        computeCommandEncoder.setBytes(&insideFactor, length: MemoryLayout<Float>.size, at: 1)
        computeCommandEncoder.setBuffer(tessellationFactorsBuffer, offset: 0, at: 2)
        computeCommandEncoder.dispatchThreadgroups(MTLSize(width: 1, height: 1, depth: 1), threadsPerThreadgroup: MTLSize(width: 1, height: 1, depth: 1))
        computeCommandEncoder.popDebugGroup()
        computeCommandEncoder.endEncoding()
    }
    
    func tessellateAndRender(view:MTKView,commandBuffer:MTLCommandBuffer) {
        let renderCommandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor:  view.currentRenderPassDescriptor!)
        renderCommandEncoder.label = "Render Command Encoder"
        renderCommandEncoder.pushDebugGroup("Tessellate and Render")
        if patchType == .triangle {
            renderCommandEncoder.setRenderPipelineState(renderPipelineStateTrangle!)
            renderCommandEncoder.setVertexBuffer(controlPointsBufferTrangle, offset: 0, at: 0)
        } else {
            renderCommandEncoder.setRenderPipelineState(renderPipelineStateQuad!)
            renderCommandEncoder.setVertexBuffer(controlPointsBufferQuad, offset: 0, at: 0)
        }
        if wireframe  {
            renderCommandEncoder.setTriangleFillMode(.lines)
        }
        //Encode tessellatin-specific commands
        renderCommandEncoder.setTessellationFactorBuffer(tessellationFactorsBuffer, offset: 0, instanceStride: 0)
        let patchControlPoints = (patchType == .triangle) ? 3 : 4
        renderCommandEncoder.drawPatches(numberOfPatchControlPoints: patchControlPoints, patchStart: 0, patchCount: 1, patchIndexBuffer: nil, patchIndexBufferOffset: 0, instanceCount: 1, baseInstance: 0)
        
        //All render commands have been encoded
        renderCommandEncoder.popDebugGroup()
        renderCommandEncoder.endEncoding()
        //Schedule a present once the drawable has been completely rendered to
        commandBuffer.present(view.currentDrawable!)
    }
    
    func draw(in view: MTKView) {
        let buffer = commandQueue.makeCommandBuffer()
        buffer.label = "Tessellation Pass"
        computeTessellationFactors(commandBuffer: buffer)
        tessellateAndRender(view: view, commandBuffer: buffer)
        buffer.commit()
    }
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        
    }
}
