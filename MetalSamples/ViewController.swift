

//
//  ViewController.swift
//  MetalSamples
//
//  Created by Apple on 2017/2/21.
//  Copyright © 2017年 Apple. All rights reserved.
//
import SceneKit
import UIKit

let TextTure = #imageLiteral(resourceName: "texture.png")
class Section {
    var title:String
    var rows:[Row]
    
    init(title:String,rows:[Row] = []) {
        self.title = title
        self.rows = rows
    }
    func addRow(title:String,handler:@escaping (ViewController)->Void) {
        rows.append(Row(title: title, action: handler))
    }
}
class Row {
    var title:String
    var action:(ViewController)->Void
    init(title:String,action:@escaping (ViewController)->Void) {
        self.title = title
        self.action = action
    }
}
class ViewController: UITableViewController {

    var sections:[Section] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Apple 3D samples"
        sections.append(MetalKitSection())
        sections.append(SceneSection())
        sections.append(CurvesSection())
        sections.append(LinesSection())
        sections.append(MovementSection())
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].rows.count
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sections[indexPath.section].rows[indexPath.row].action(self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "item")
        cell?.textLabel?.text = sections[indexPath.section].rows[indexPath.row].title
        return cell!
    }
}
