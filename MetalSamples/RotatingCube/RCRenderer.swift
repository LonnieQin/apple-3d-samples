//
//  RCRenderer.swift
//  MetalSamples
//
//  Created by Apple on 2017/2/21.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit
import simd
import MetalKit

struct RCConstants {
    var modelViewProjectionMatrix = matrix_identity_float4x4
    var normalMatrix = matrix_identity_float3x3
    var time:Float = 0
    var meshSize:float3 = .init()
}
class RCRenderer: NSObject,MTKViewDelegate {
    weak var view:MTKView!
    let device:MTLDevice
    let commandQueue:MTLCommandQueue
    let renderPipelineState:MTLRenderPipelineState
    let depthStencilState:MTLDepthStencilState
    let sampler:MTLSamplerState
    let texture:MTLTexture
    let mesh:RCMesh
    var time = TimeInterval(0.0)
    var constants = RCConstants()
    
    init?(mtkView:MTKView) {
        view = mtkView
        view.sampleCount = 4
        view.clearColor = MTLClearColorMake(1, 1, 1, 1)
        view.colorPixelFormat = .bgra8Unorm
        view.depthStencilPixelFormat = .depth32Float
        if let defaultDevice = MTLCreateSystemDefaultDevice() {
            device = defaultDevice
        } else {
            print("Metal is not supported")
            return nil
        }
        
        //Create the command queue we will be using to submit work to the GPU
        commandQueue = device.makeCommandQueue()
        
        //Compile the functions and other state into a pipeline state object.
        do {
            renderPipelineState = try RCRenderer.buildRenderPipelineStateWithDevice(device, view: view)
        }
        catch {
            print("Unable to compile render pipeline state")
            return nil
        }
        let icon = #imageLiteral(resourceName: "headicon")
        let m = max(icon.size.width, icon.size.height)
        mesh = RCMesh(cubeWithSize: .init(Float(icon.size.width/m), Float(icon.size.height/m), Float(icon.size.width/m)), device: device)!
        constants.meshSize = .init(Float(icon.size.width/m), Float(icon.size.height/m), Float(icon.size.width/m))
        do {
            texture = try RCRenderer.buildTexture(name: "headicon", device)
        }
        catch {
            return nil
        }
        
        //Make a depth-stencil state that passes when fragments are nearer to the camera that previous fragments
        depthStencilState = RCRenderer.buildDepthStencilStateWithDevice(device, compareFunc: .less, isWriteEnabled: true)
        
        //Make a texture sampler that wraps in both directions and performs bilinear filtering
        sampler = RCRenderer.buildSamplerStateWithDevice(device, addressMode: .repeat, filter: .linear)
        
        super.init()
        view.delegate = self
        view.device = device
    }
    
    class func buildRenderPipelineStateWithDevice(_ device:MTLDevice, view:MTKView) throws -> MTLRenderPipelineState{
        let lib = device.newDefaultLibrary()!
        let vertexFunction = lib.makeFunction(name: "vertex_transform")
        let fragmentFunction = lib.makeFunction(name: "fragment_lit_textured")
        
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.label = "Render Pipeline"
        pipelineDescriptor.sampleCount = view.sampleCount
        pipelineDescriptor.vertexFunction = vertexFunction
        pipelineDescriptor.fragmentFunction = fragmentFunction
        pipelineDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat
        pipelineDescriptor.depthAttachmentPixelFormat = view.depthStencilPixelFormat
        return try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
    }
    
    class func buildTexture(name:String,_ device:MTLDevice) throws ->MTLTexture {
        let textureLoader = MTKTextureLoader(device: device)
        let asset = NSDataAsset(name: name)
        if let data = asset?.data {
            return try textureLoader.newTexture(with: data, options: [:])
        } else {
            let data = UIImagePNGRepresentation(UIImage(named: name)!)
            return try textureLoader.newTexture(with: data!, options: [:])
        }
    }
    
    class func buildSamplerStateWithDevice(_ device:MTLDevice,addressMode:MTLSamplerAddressMode,filter:MTLSamplerMinMagFilter)->MTLSamplerState {
        let samplerDescriptor = MTLSamplerDescriptor()
        samplerDescriptor.sAddressMode = addressMode
        samplerDescriptor.tAddressMode = addressMode
        samplerDescriptor.minFilter = filter
        samplerDescriptor.magFilter = filter
        return device.makeSamplerState(descriptor: samplerDescriptor)
    }
    
    class func buildDepthStencilStateWithDevice(_ device:MTLDevice,compareFunc:MTLCompareFunction,isWriteEnabled:Bool)->MTLDepthStencilState {
        let desc = MTLDepthStencilDescriptor()
        desc.depthCompareFunction = compareFunc
        desc.isDepthWriteEnabled = isWriteEnabled
        return device.makeDepthStencilState(descriptor: desc)
    }
    
    
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        
    }
    
    func updateWithTimeStep(_ timestep:TimeInterval) {
        time = time + timestep
       
        let modelToWorldMatrix = matrix4x4_rotation(Float(time)*0.5,vector_float3(0.7,1,0))
        let viewSize = self.view.bounds.size
        let aspectRatio = Float(viewSize.width/viewSize.height)
        let verticalViewAngle = radians_from_degrees(65)
        let nearZ:Float = 0.1
        let farZ:Float = 100.0
        
        let projectionMatrix = matrix_perspective(verticalViewAngle, aspectRatio, nearZ, farZ)
        
        let viewMatrix = matrix_look_at(0, 0, 2.5, 0, 0, 0, 0, 1, 0)
        
 
        let mvMatrix = matrix_multiply(viewMatrix, modelToWorldMatrix)
 
        constants.modelViewProjectionMatrix = matrix_multiply(projectionMatrix, mvMatrix)
        constants.normalMatrix = matrix_inverse_transpose(matrix_upper_left_3x3(mvMatrix))
        
        constants.time = Float(time)
    }
    
    func render(_ view:MTKView) {
        let timestep = 1.0 / TimeInterval(view.preferredFramesPerSecond)
        updateWithTimeStep(timestep)
        
        let commandBuffer = commandQueue.makeCommandBuffer()
        let renderPassDescriptor = view.currentRenderPassDescriptor
        
        if let renderPassDescriptor = renderPassDescriptor {
            let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)
            renderEncoder.pushDebugGroup("Draw Cube")
            renderEncoder.setFrontFacing(.counterClockwise)
            renderEncoder.setDepthStencilState(depthStencilState)
            renderEncoder.setRenderPipelineState(renderPipelineState)
            renderEncoder.setVertexBuffer(mesh.vertexBuffer, offset: 0, at: 0)
            //Bind the uniform buffer so we can read our model-view-projection matrix in the shader
            renderEncoder.setVertexBytes(&constants, length: MemoryLayout<RCConstants>.size, at: 1)
            //Bind our texture so we can sample from it in the fragment shader
            renderEncoder.setFragmentTexture(texture, at: 0)
            //Bind our sampler state so we can use it to sample the texture in the fragment shader
            renderEncoder.setFragmentSamplerState(sampler, at: 0)
            //Issue the draw call to draw the indexed geometry of the mesh
            renderEncoder.drawIndexedPrimitives(type: mesh.primitiveType, indexCount: mesh.indexCount, indexType: mesh.indexType, indexBuffer: mesh.indexBuffer, indexBufferOffset: 0)
            renderEncoder.popDebugGroup()
            renderEncoder.endEncoding()
            if let drawable = view.currentDrawable {
                commandBuffer.present(drawable)
            }
        }
        commandBuffer.addCompletedHandler { (buffer) in
           
        }
        
        commandBuffer.commit()
    }
    
    func draw(in view: MTKView) {
        render(view)
    }
}
