//
//  RCViewController.swift
//  MetalSamples
//
//  Created by Apple on 2017/2/21.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit
import MetalKit
class RCViewController: UIViewController {
    var renderer: RCRenderer!
    override func viewDidLoad() {
        super.viewDidLoad()
        let metalView = self.view as! MTKView 
        renderer = RCRenderer(mtkView:metalView)
    }
 
}
