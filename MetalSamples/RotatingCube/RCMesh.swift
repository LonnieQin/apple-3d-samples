//
//  RCMesh.swift
//  MetalSamples
//
//  Created by Apple on 2017/2/21.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit
import Metal
import MetalKit
import ModelIO
class RCMesh: NSObject {
    var vertexBuffer:MTLBuffer
    var vertexDescriptor:MTLVertexDescriptor
    var primitiveType:MTLPrimitiveType
    var indexBuffer:MTLBuffer
    var indexCount:Int
    var indexType:MTLIndexType
    init?(cubeWithSize size:vector_float3,device:MTLDevice) {
        let allocator = MTKMeshBufferAllocator(device: device)
        let mdlMesh = MDLMesh(boxWithExtent: size, segments: vector_uint3(UInt32(size.x/0.01),UInt32(size.y/0.01),UInt32(size.z/0.01)), inwardNormals: false, geometryType: .triangles, allocator: allocator)
        do {
            let mtkMesh = try MTKMesh(mesh: mdlMesh, device: device)
            let mtkVertexBuffer = mtkMesh.vertexBuffers[0]
            let submesh = mtkMesh.submeshes[0]
            let mtkIndexBuffer = submesh.indexBuffer
            
            vertexBuffer = mtkVertexBuffer.buffer
            vertexBuffer.label = "Mesh Vertices"
            
            vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(mdlMesh.vertexDescriptor)
            primitiveType = submesh.primitiveType
            indexBuffer = mtkIndexBuffer.buffer
            indexBuffer.label = "Mesh Indices"
            indexCount = submesh.indexCount
            indexType = submesh.indexType
        } catch _ {
            return nil
        }
        
    }
}
