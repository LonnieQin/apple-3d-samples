//
//  CubesViewController.swift
//  ThreeDSamples
//
//  Created by Apple on 2017/2/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit

class CubesViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let metalView = UINib.init(nibName: "MTKView", bundle: nil).instantiate(withOwner: nil, options: nil).first {$0 is CubeMetalView} as! CubeMetalView
        metalView.frame = view.bounds
        view.addSubview(metalView)
    }
 

}
