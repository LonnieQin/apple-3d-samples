//
//  CarGameView.swift
//  MetalSamples
//
//  Created by Apple on 2017/2/22.
//  Copyright © 2017年 Apple. All rights reserved.
//

import SceneKit
class CarGameView: SCNView {
    var touchCount = 0
    var inCarView = false
    func changePointOfView() {
        let pointOfViews = self.scene!.rootNode.childNodes(passingTest: { (node, stop) -> Bool in
            return (node.camera != nil)
        })
        var index = pointOfViews.index(of: self.pointOfView!)!
        index += 1
        index %= pointOfViews.count
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 0.75
        self.pointOfView = pointOfViews[index]
        SCNTransaction.commit()
        inCarView = (index == 0)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let p = self.overlaySKScene!.convertPoint(fromView: touches.first!.location(in: self))
        if  overlaySKScene!.nodes(at: p).contains(where: {$0.name == "camera"}) {
            overlaySKScene!.run(.playSoundFileNamed("click.caf", waitForCompletion: false), completion: { 
                self.changePointOfView()
            })
        }
        touchCount = event!.allTouches!.count
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchCount = 0
    }
}
