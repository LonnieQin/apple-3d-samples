//
//  CarGameViewController.swift
//  MetalSamples
//
//  Created by Apple on 2017/2/22.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit
import SceneKit
import SpriteKit
import simd
import CoreMotion 
let MAX_SPEED:Double = 250

class CarGameViewController: UIViewController,SCNSceneRendererDelegate  {
    
    @IBOutlet var gameView: CarGameView!
    static func instance()->CarGameViewController {
        return UIStoryboard(name: "CarGameViewController", bundle: nil).instantiateInitialViewController() as! CarGameViewController
    }
    let scene = SCNScene()
    private var sportLightNode:SCNNode = SCNNode()
    private var cameraNode:SCNNode = SCNNode()
    private var vehicle:SCNPhysicsVehicle = SCNPhysicsVehicle()
    private var vehicleNode:SCNNode = SCNNode()
    private var reactor:SCNParticleSystem = SCNParticleSystem()
    private var reactorDefaultBirthRate:CGFloat = 0.0
    private var vehicleSteering:CGFloat = 0
    
    lazy var motionManager = CMMotionManager()
    
    required init?(coder aDecoder: NSCoder) {
        //Add an ambient light
        let ambientLight = SCNNode()
        ambientLight.light = SCNLight()
        ambientLight.light?.type = .ambient
        ambientLight.light?.color = UIColor(white: 0.3, alpha: 1)
        scene.rootNode.addChildNode(ambientLight)

        //Global Environment
        sportLightNode = SCNNode()
        sportLightNode.light = SCNLight()
        sportLightNode.light?.type = .spot
        sportLightNode.light?.castsShadow = true
        sportLightNode.light?.color = UIColor(white: 0.8, alpha: 1)
        sportLightNode.position = SCNVector3(0, 80, 30)
        sportLightNode.rotation = SCNVector4(1, 0, 0, -M_PI/2.8)
        sportLightNode.light?.spotInnerAngle = 0
        sportLightNode.light?.spotOuterAngle = 50
        sportLightNode.light?.shadowColor = UIColor.black
        sportLightNode.light?.zFar = 500
        sportLightNode.light?.zNear = 50
        scene.rootNode.addChildNode(sportLightNode)
        
        let floor = SCNNode()
        floor.geometry = SCNFloor()
        floor.geometry?.firstMaterial?.diffuse.contents = "wood.png"
        floor.geometry?.firstMaterial?.diffuse.contentsTransform = SCNMatrix4MakeScale(2, 2, 1)
        floor.geometry?.firstMaterial?.locksAmbientWithDiffuse = true
        (floor.geometry as! SCNFloor).reflectionFalloffEnd = 10
        let body = SCNPhysicsBody.static()
        floor.physicsBody = body
        scene.rootNode.addChildNode(floor)
        //Add Elements
        
        
        
        //Setup Vehicle
        
        //Create a main Camera
        cameraNode.camera = SCNCamera()
        cameraNode.camera!.zFar = 500
        cameraNode.position = SCNVector3Make(0, 60, 50)
        let f = -M_PI_4 * 0.75
        cameraNode.rotation = SCNVector4Make(0, 0, 0, Float(f))
        scene.rootNode.addChildNode(cameraNode)
        //Add a secondary camera to the car
        
        super.init(coder: aDecoder)
        setupScene()
        setupVehicle()
    }
    
    func addTrainAtPosition(pos:SCNVector3) {
        let trainScene = SCNScene(named: "train_flat")
        //Physicalize the train with simple boxes
        trainScene?.rootNode.childNodes.forEach({[unowned self] node in
            if node.geometry != nil{
                node.position = SCNVector3(node.position.x+pos.x, node.position.y+pos.y, node.position.z+pos.z)
                let body = SCNPhysicsBody.dynamic()
                let boundingBox = node.boundingBox
                let min = boundingBox.min
                let max = boundingBox.max
                let boxShape  = SCNBox(width: CGFloat(max.x-min.x), height: CGFloat(max.y-min.y), length: CGFloat(max.z-min.z), chamferRadius: 0.0)
                body.physicsShape = SCNPhysicsShape(geometry: boxShape, options: nil)
                node.pivot = SCNMatrix4MakeTranslation(0, -min.y, 0)
                node.physicsBody = body
                self.scene.rootNode.addChildNode(node)
            }
        })
        
        //add smoke 
        let smokeHandle = scene.rootNode.childNode(withName: "Smoke", recursively: true)
        smokeHandle?.addParticleSystem(SCNParticleSystem(named: "smoke", inDirectory: nil)!)
        //Add physicas constraints between engiine and wagons
        //let engineCar = scene.rootNode.childNode(withName: "EngineCar", recursively: false)
       // let wagon1 = scene.rootNode.childNode(withName: "Wagon1", recursively: false)
        //let wagon2 = scene.rootNode.childNode(withName: "Wagon2", recursively: false)
        //let (min,max) = engineCar!.boundingBox
        //let (wmin,wmax) = engineCar!.boundingBox
       // wagon1?.runAction(.sequence([.wait(duration: 1),.moveBy(function:Parameter3DFunction(x: 20*sin(x), y: Number(0), z: Number(0)))]))
        //wagon1?.runAction(.sequence([.wait(duration: 1),.moveBy(function:Parameter3DFunction(x: 20*sin(t)+10, y: 20*cos(t)+10, z: Number(10)))]))
        //scene.physicsWorld.addBehavior(SCNPhysicsBallSocketJoint(bodyA: engineCar!.physicsBody!, anchorA: SCNVector3Make(max.x, min.y, 0), bodyB: wagon1!.physicsBody!, anchorB: SCNVector3Make(wmin.x, wmin.y, 0)))
        
        //scene.physicsWorld.addBehavior(SCNPhysicsBallSocketJoint(bodyA: wagon1!.physicsBody!, anchorA: SCNVector3Make(wmax.x+0.1, wmin.y, 0), bodyB: wagon2!.physicsBody!, anchorB: SCNVector3Make(wmin.x-0.1, wmin.y, 0)))
    }
    
    
    func setupScene() {
        //Add train
        addTrainAtPosition(pos: SCNVector3Make(-5, 20, -40))
        addWoodBlock(imageName: "WoodCubeA.jpg", position: SCNVector3Make(-10, 15, 10))
        addWoodBlock(imageName: "WoodCubeB.jpg", position: SCNVector3Make(-9, 10, 10))
        addWoodBlock(imageName: "WoodCubeC.jpg", position: SCNVector3Make(20, 15, -11))
        addWoodBlock(imageName: "WoodCubeA.jpg", position: SCNVector3Make(25, 5, -20))
        
        //add walls
        var wall = SCNNode(geometry: SCNBox(width: 400, height: 100, length: 4, chamferRadius: 0))
        wall.geometry?.firstMaterial?.diffuse.contents = "wall.jpg"
        wall.geometry?.firstMaterial?.diffuse.contentsTransform = SCNMatrix4Mult(SCNMatrix4MakeScale(24, 2, 1), SCNMatrix4MakeTranslation(0, 1, 0))
        wall.geometry?.firstMaterial?.diffuse.wrapS = .repeat
        wall.geometry?.firstMaterial?.diffuse.wrapT = .mirror
        wall.geometry?.firstMaterial?.isDoubleSided = false
        wall.castsShadow = false
        wall.position = .init(0, 50, -92)
        wall.physicsBody = .static()
        wall.geometry?.firstMaterial?.locksAmbientWithDiffuse = true
        scene.rootNode.addChildNode(wall)
        
        wall = wall.clone()
        wall.position = SCNVector3Make(-202, 50, 0)
        wall.rotation = SCNVector4Make(0, 1, 0, Float(M_PI_2))
        scene.rootNode.addChildNode(wall)
        
        wall = wall.clone()
        wall.position = SCNVector3Make(202, 50, 0)
        wall.rotation = SCNVector4Make(0, 1, 0, -Float(M_PI_2))
        scene.rootNode.addChildNode(wall)
        
        //Add Ceil
        let backWall = SCNNode(geometry: SCNPlane(width: 400, height: 100))
        backWall.geometry?.firstMaterial = wall.geometry?.firstMaterial
        backWall.position = .init(0, 50, 200)
        backWall.rotation = .init(0, 1, 0, M_PI)
        backWall.castsShadow = false
        backWall.physicsBody = .static()
        scene.rootNode.addChildNode(backWall)
        
        
        
        //Add wooden blocks
        for _ in 0..<4 {
            
            addWoodBlock(imageName: "WoodCubeA.jpg", position: SCNVector3(Float(Int(arc4random())%60 - 30), 20, Float(Int(arc4random())%40 - 20)))
            addWoodBlock(imageName: "WoodCubeB.jpg", position: SCNVector3(Float(Int(arc4random())%60 - 30), 20, Float(Int(arc4random())%40 - 20)))
            addWoodBlock(imageName: "WoodCubeC.jpg", position: SCNVector3(Float(Int(arc4random())%60 - 30), 20, Float(Int(arc4random())%40 - 20)))
        }
        
        //add cartoon book
        let block = SCNNode()
        block.position = .init(20, 10, -16)
        block.rotation = .init(0, 1, 0, -M_PI_4)
        block.geometry = SCNBox(width: 22, height: 0.2, length: 34, chamferRadius: 0)
        let fontMat = SCNMaterial()
        fontMat.locksAmbientWithDiffuse = true
        fontMat.diffuse.contents = "book_front.jpg"
        fontMat.diffuse.mipFilter = .linear
        let backMat = SCNMaterial()
        backMat.locksAmbientWithDiffuse = true
        backMat.diffuse.contents = "book_back.jpg"
        backMat.diffuse.mipFilter = .linear
        block.geometry?.materials = [fontMat,backMat]
        block.physicsBody = .dynamic()
        scene.rootNode.addChildNode(block)
        
        // add carpet
        let rug = SCNNode()
        rug.position = .init(0, 0.01, 0)
        rug.rotation = .init(1, 0, 0, CGFloat(M_PI))
        let path = UIBezierPath(roundedRect: .init(x: -50, y: -30, width: 100, height: 50), cornerRadius: 2.5)
        path.flatness = 0.1
        rug.geometry = SCNShape(path: path, extrusionDepth: 0.05)
        rug.geometry?.firstMaterial?.locksAmbientWithDiffuse = true
        rug.geometry?.firstMaterial?.diffuse.contents = "carpet.jpg"
        scene.rootNode.addChildNode(rug)
        
        //add ball
        let ball = SCNNode()
        ball.position = .init(-5, 5, -18)
        ball.geometry = SCNSphere(radius: 5)
        ball.geometry?.firstMaterial?.locksAmbientWithDiffuse = true
        ball.geometry?.firstMaterial?.diffuse.contents = "ball.jpg"
        ball.geometry?.firstMaterial?.diffuse.contentsTransform = SCNMatrix4MakeScale(2, 1, 1)
        ball.geometry?.firstMaterial?.diffuse.wrapS = .mirror
        ball.physicsBody?.restitution = 0.9
        scene.rootNode.addChildNode(ball)
        
        
    }
    
    func setupVehicle() {
        let carScene = SCNScene(named: "rc_car")
        let chassisNode = carScene!.rootNode.childNode(withName: "rccarBody", recursively: false)!
        chassisNode.position = SCNVector3(-5, 20, -40)
        chassisNode.rotation = SCNVector4(0,1,0,M_PI)
        let body = SCNPhysicsBody.dynamic()
        body.allowsResting = false
        body.mass = 80
        body.restitution = 0.1
        body.friction = 0.5
        body.rollingFriction = 0
        chassisNode.physicsBody = body
        scene.rootNode.addChildNode(chassisNode)
        
        let pipeNode = chassisNode.childNode(withName: "pipe", recursively: true)
        reactor = SCNParticleSystem(named: "smoke", inDirectory: nil)!
        reactorDefaultBirthRate = reactor.birthRate
        reactor.birthRate = 0
        pipeNode?.addParticleSystem(reactor)
        
        //Add Wheels
        let wheel0Node = chassisNode.childNode(withName: "wheelLocator_FL", recursively: true)
        let wheel1Node = chassisNode.childNode(withName: "wheelLocator_FR", recursively: true)
        let wheel2Node = chassisNode.childNode(withName: "wheelLocator_RL", recursively: true)
        let wheel3Node = chassisNode.childNode(withName: "wheelLocator_RR", recursively: true)
        let wheel0 = SCNPhysicsVehicleWheel(node: wheel0Node!)
        let wheel1 = SCNPhysicsVehicleWheel(node: wheel1Node!)
        let wheel2 = SCNPhysicsVehicleWheel(node: wheel2Node!)
        let wheel3 = SCNPhysicsVehicleWheel(node: wheel3Node!)
        let (min,max) = wheel0Node!.boundingBox
        let halfWidth = 0.5 * (max.x-min.x)
 
        wheel0.connectionPosition = SCNVector3FromFloat3(SCNVector3ToFloat3(wheel0Node!.convertPosition(SCNVector3Zero, to: chassisNode))+vector_float3(halfWidth, 0, 0))
        wheel1.connectionPosition = SCNVector3FromFloat3(SCNVector3ToFloat3(wheel1Node!.convertPosition(SCNVector3Zero, to: chassisNode))-vector_float3(halfWidth, 0, 0))
        wheel2.connectionPosition = SCNVector3FromFloat3(SCNVector3ToFloat3(wheel2Node!.convertPosition(SCNVector3Zero, to: chassisNode))+vector_float3(halfWidth, 0, 0))
        wheel3.connectionPosition = SCNVector3FromFloat3(SCNVector3ToFloat3(wheel3Node!.convertPosition(SCNVector3Zero, to: chassisNode))+vector_float3(halfWidth, 0, 0))
        
        vehicle = SCNPhysicsVehicle(chassisBody: chassisNode.physicsBody!, wheels: [wheel0,wheel1,wheel2,wheel3])
        scene.physicsWorld.addBehavior(vehicle)
        vehicleNode = chassisNode
        
        let frontCameraNode = SCNNode()
        frontCameraNode.position = SCNVector3Make(0, 3.5, 2.5)
        frontCameraNode.rotation = SCNVector4Make(0, 1, 0, Float(M_PI))
        frontCameraNode.camera = SCNCamera()
        frontCameraNode.camera?.xFov = 75
        frontCameraNode.camera?.zFar = 500
        vehicleNode.addChildNode(frontCameraNode)
    }
    
    func addWoodBlock(imageName:String,position:SCNVector3) {
        let block = SCNNode()
        block.position = position
        block.geometry = SCNBox(width: 5, height: 5, length: 5, chamferRadius: 0)
        block.geometry?.firstMaterial?.diffuse.contents = imageName
        block.geometry?.firstMaterial?.diffuse.mipFilter = .linear
        block.physicsBody = .dynamic()
        scene.rootNode.addChildNode(block)
    }
   
    
    func renderer(_ renderer: SCNSceneRenderer, didSimulatePhysicsAtTime time: TimeInterval) {
        let defaultEngineForce:CGFloat = 300
        let defaultBrakingForce:CGFloat = 3.0
        let steeringClamp = 0.5
        let cameraDamping = 0.3
        
        var engineForce:CGFloat = 0
        var brakingForce:CGFloat = 0
        
        var orient = self.orientation
        if gameView.touchCount == 1 {
            engineForce = defaultEngineForce
            reactor.birthRate = reactorDefaultBirthRate
        }
        else if gameView.touchCount == 2 {
            engineForce = -defaultEngineForce
            reactor.birthRate = 0
        }
        else if gameView.touchCount == 3 {
            brakingForce = 100
            reactor.birthRate = 0
        } else {
            brakingForce = defaultBrakingForce
        } 
        vehicleSteering = -CGFloat(orientation)
        vehicle.setSteeringAngle(vehicleSteering, forWheelAt: 0)
        vehicle.setSteeringAngle(vehicleSteering, forWheelAt: 1)
        vehicle.applyEngineForce(engineForce, forWheelAt: 2)
        vehicle.applyEngineForce(engineForce, forWheelAt: 3)
        vehicle.applyBrakingForce(brakingForce, forWheelAt: 2)
        vehicle.applyEngineForce(brakingForce, forWheelAt: 3)
        reorientCarIfNeeded()
        let car = vehicleNode.presentation
        let carPos = car.position
        let targetPos = vector_float3.init(carPos.x, 30, carPos.z+25)
        var cameraPos = SCNVector3ToFloat3(cameraNode.position)
        
        cameraPos = vector_mix(cameraPos, targetPos, vector_float3(Float(cameraDamping)))
        cameraNode.position = SCNVector3FromFloat3(cameraPos)
        if gameView.inCarView {
            let frontPosition = gameView.pointOfView!.presentation.convertPosition(.init(0, 0, -30), to: nil)
            sportLightNode.position = .init(frontPosition.x, 80, frontPosition.z)
            sportLightNode.rotation = .init(1, 0, 0, -M_PI/2.0)
        } else {
            sportLightNode.position = .init(carPos.x, 80, carPos.z+30)
            sportLightNode.rotation = .init(1, 0, 0, -M_PI/2.8)
        }
        
        let scene = gameView.overlaySKScene as! CarOverlayScene
        scene.speedNeedle.zPosition = -(vehicle.speedInKilometersPerHour * CGFloat(M_PI) / CGFloat(MAX_SPEED))
    }
    
    var ticks = 0
    var check = 0
    var tryTime = 0
    func reorientCarIfNeeded() {
        let car = vehicleNode.presentation
        let carPos = car.position
        ticks += 1
        if ticks == 30 {
            let t = car.worldTransform
            if t.m22 <= 0.1 {
                check += 1
                if check == 3 {
                    tryTime += 1
                    if tryTime == 3 {
                        tryTime = 0
                        vehicleNode.rotation = .init()
                        vehicleNode.position = .init(carPos.x, carPos.y, carPos.z)
                        vehicleNode.physicsBody?.resetTransform()
                    } else {
                        let pos = SCNVector3Make(-10*(Float(Int(arc4random()) % 100) / 100.0-0.5),0, -10*(Float(Int(arc4random()) % 100) / 100.0-0.5))
                        vehicleNode.physicsBody?.applyForce(.init(0, 300, 0), at: pos, asImpulse: true)
                    }
                    check = 0
                }
            } else {
                check = 0
            }
        } else {
            ticks = 0
        }
    }
 
    #if os(iOS)
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isStatusBarHidden = true
        gameView.backgroundColor = .black
        gameView.scene = scene
        gameView.scene?.physicsWorld.speed = 4.0
        gameView.overlaySKScene = CarOverlayScene(size:gameView.bounds.size)
        gameView.pointOfView = cameraNode
        gameView.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(CarGameViewController.doubleTap(g:)))
        tap.numberOfTapsRequired = 2
        tap.numberOfTouchesRequired = 2
        gameView.gestureRecognizers = [tap]
 
    }
    
    
    var vec:float3 = float3()
    var orientation:Float = 0
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // MotionManager
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = 1/60.0
            let kFilteringFactor:Float = 0.5
            motionManager.startAccelerometerUpdates(to: .main, withHandler: {[unowned self] (data, error) in
                if data == nil {return}
                self.vec.x = Float(data!.acceleration.x) * kFilteringFactor + self.vec.x * (1.0-kFilteringFactor)
                self.vec.y = Float(data!.acceleration.y) * kFilteringFactor + self.vec.y * (1.0-kFilteringFactor)
                self.vec.z = Float(data!.acceleration.z) * kFilteringFactor + self.vec.z * (1.0-kFilteringFactor)
                if self.vec.x > 0 {
                    self.orientation = self.vec.x * 1.3
                } else {
                    self.orientation = -1.0 * self.vec.x * 1.3
                }
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        motionManager.stopAccelerometerUpdates()
    }
 
    func doubleTap(g:UITapGestureRecognizer) {
    
    }
    
    #endif
 
}
