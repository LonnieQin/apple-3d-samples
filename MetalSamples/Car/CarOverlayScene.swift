//
//  CarOverlayScene.swift
//  MetalSamples
//
//  Created by Apple on 2017/2/22.
//  Copyright © 2017年 Apple. All rights reserved.
//

import SpriteKit
class CarOverlayScene: SKScene {
    let speedNeedle:SKNode
    override init(size: CGSize) {
        speedNeedle = SKNode()
        super.init(size: size)
        self.anchorPoint = .init(x: 0.5, y: 0.5)
        self.scaleMode = .fill
        
        let scale:CGFloat =  (UIDevice.current.userInterfaceIdiom == .pad)  ? 1.5:1.0
        let myImage = SKSpriteNode(imageNamed: "speedGauge.png")
        myImage.position = .init(x: size.width*0.33, y: -size.height*0.5)
        myImage.anchorPoint = .init(x: 0.5, y: 0)
        myImage.xScale = 0.8 * scale
        myImage.yScale = 0.8 * scale
        addChild(myImage)
        let needle = SKSpriteNode(imageNamed: "needle.png")
        speedNeedle.position = .init(x: 0, y: 16)
        needle.anchorPoint = .init(x: 0.5, y: 0)
        needle.xScale = 0.7
        needle.yScale = 0.7
        needle.zPosition = CGFloat(M_PI_2)
        speedNeedle.addChild(needle)
        myImage.addChild(speedNeedle)
        let cameraImage = SKSpriteNode(imageNamed: "video_camera.png")
        cameraImage.position = .init(x: -size.width*0.4, y: -size.height*0.4)
        cameraImage.name = "camera"
        cameraImage.xScale = 0.6 * scale
        cameraImage.yScale = 0.6 * scale
        addChild(cameraImage)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
