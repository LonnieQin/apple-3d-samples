//
//  BlockAction.swift
//  HighScoreTalk
//
//  Created by Apple on 2017/1/10.
//  Copyright © 2017年 apple. All rights reserved.
//

import Foundation

/// 执行Block内容 设置finished为true时Action结束
class BlockAction:Action {
    var block:(BlockAction)->Void = {(action) in
    
    }
    init(block:@escaping (BlockAction)->Void) {
        self.block = block
        super.init()
    }
    
    override func execute(){
        block(self)
    }
    
    override func stop() {
        finished = true
    }
}
