//
//  GroupAction.swift
//  HighScoreTalk
//
//  Created by Apple on 2017/1/10.
//  Copyright © 2017年 apple. All rights reserved.
//

import Foundation

/// 并发执行多个Action,并且在每一个Action都执行完后调用shouldFinish语句块决定Action是否结束，一般是根据Action的完成情况判断。
class GroupAction:Action {
    
    /// Action数组
    private let actions:NSArray
    
    /// 是否完成的语句块
    let shouldFinishBlock:([Action])->Bool
    
    /// 初始化
    ///
    /// - Parameters:
    ///   - actions: Action参数列表
    ///   - shouldFinish: 是否完成的语句块。
    init(_ actions:Action...,shouldFinish:@escaping ([Action])->Bool) {
        self.actions = NSArray(array: actions)
        self.shouldFinishBlock = shouldFinish
        super.init()
        //监听Action是否结束
        self.actions.addObserver(self, toObjectsAt: IndexSet(integersIn:0..<self.actions.count), forKeyPath: "finished", options: .new, context: nil)
    }
    override func execute() {
        self.actions.forEach { (action) in
            (action as! Action).execute()
        }
    }
    
    override func cancel() {
        super.cancel()
        actions.forEach { (action) in
            (action as! Action).cancel()
        }
    }
    
    override func stop() {
        
        if finished == false {
            finished = true
            actions.forEach { (action) in
                if (action as! Action).finished == false {
                    (action as! Action).stop()
                }
            }
        }
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if shouldFinishBlock(self.actions as! [Action]) && finished == false{
            stop()
        }
    }
    deinit {
        self.actions.removeObserver(self, fromObjectsAt: IndexSet(integersIn:0..<self.actions.count), forKeyPath: "finished")
    }
}


func && (action1:Action,action2:Action)->GroupAction {
    return GroupAction(action1,action2,shouldFinish:{$0[0].finished && $0[1].finished})
}
func || (action1:Action,action2:Action)->GroupAction  {
    return GroupAction(action1,action2,shouldFinish:{$0[0].finished || $0[1].finished})
}
prefix func ! (action:Action)->GroupAction  {
    return GroupAction(action,shouldFinish:{!$0[0].finished})
}

prefix operator ✓
prefix func ✓ (action:Action)->GroupAction  {
    return GroupAction(action,shouldFinish:{_ in true})
}
prefix operator ✕
prefix func ✕ (action:Action)->GroupAction  {
    return GroupAction(action,shouldFinish:{_ in false})
}
