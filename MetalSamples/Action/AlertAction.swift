//
//  AlertAction.swift
//  TouTou
//
//  Created by Apple on 2017/1/11.
//  Copyright © 2017年 Apple. All rights reserved.
//

import Foundation
struct AlertParam {
    var title:String
    var style:UIAlertActionStyle
    var block:(AlertParam)->Void
}
class AlertAction:Action {
    var title:String
    var message:String
    var params:[AlertParam]
    var delay:Double
    init(title:String = "提示",message:String,params:[AlertParam] = [AlertParam(title:"确定",style:.cancel){_ in }],delay:Double = 1) {
        self.title = title
        self.message = message
        self.params = params
        self.delay = delay
        super.init()
    }
    override func execute() {
        DispatchQueue.main.asyncAfter(deadline: .now()+delay) {
            let c = UIAlertController(title: self.title, message: "\n"+self.message, preferredStyle: .alert)
            self.params.forEach({ (action) in
                c.addAction(UIAlertAction(title: action.title, style: action.style, handler: { (a) in
                    action.block(action)
                    self.finished = true
                }))
            })
            UIApplication.shared.keyWindow?.rootViewController?.present(c, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                if !self.finished {
                    self.finished = true
                    c.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
}
