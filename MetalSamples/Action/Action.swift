//
//  Action.swift
//  HighScoreTalk
//
//  Created by apple on 16/5/14.
//  Copyright © 2016年 apple. All rights reserved.
//

import Foundation

/// Action,用来执行命令
class Action:NSObject {
    /// 标签
    var tags:[String] = []
    /// 是否已取消
    var canceled = false

    /// 是否已结束
    dynamic var finished:Bool = false
    
    /// 取消
    func cancel(){
        canceled = true
    }
    
    /// 执行
    func execute(){
        
    }
    
    /// 停止
    func stop() {
    
    }
}
