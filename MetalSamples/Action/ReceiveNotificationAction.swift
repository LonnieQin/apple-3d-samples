//
//  ReceiveNotificationAction.swift
//  HighScoreTalk
//
//  Created by Apple on 2017/1/6.
//  Copyright © 2017年 apple. All rights reserved.
//

import UIKit

/// 接收通知的命令，执行的时候加通知到NotificationCenter接收到通知后移除通知，Action结束
class ReceiveNotificationAction: Action {
    var notificationName:String
    var handler:(Notification)->Void
    var notification:Notification?
    fileprivate var addNotificaion:Bool = false
    
    
    /// 初始化参数
    init(name:String,handler:@escaping (Notification)->Void = {_ in }) {
        self.notificationName = name
        self.handler = handler
        super.init()
    }
    
    override func execute() {
        NotificationCenter.default.addObserver(self, selector: #selector(ReceiveNotificationAction.onReceiveNotification(notification:)), name: Notification.Name(notificationName), object: nil)
        addNotificaion = true
    }
    
    func onReceiveNotification(notification:Notification) {
        if finished == false && canceled == false{
            self.notification = notification
            handler(notification)
            finished = true
        }
    }
    
    override func cancel() {
        super.cancel()
        if addNotificaion == true {
            NotificationCenter.default.removeObserver(self)
            addNotificaion = false
        }
    }
    
    override func stop() {
        if addNotificaion == true {
            NotificationCenter.default.removeObserver(self)
            addNotificaion = false
        }
    }
}
