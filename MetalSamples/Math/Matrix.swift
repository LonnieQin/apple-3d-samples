//
//  Matrix.swift
//  Math
//
//  Created by apple on 2016/10/11.
//  Copyright © 2016年 Lonnie. All rights reserved.
//

import UIKit

/// 矩阵
struct Matrix {
    private var values = [Double]()
    var rowCount = 0
    var colCount = 0
    init(rowCount:Int,colCount:Int) {
        self.rowCount = rowCount
        self.colCount = colCount
        values = Array(repeating: 0.0, count: rowCount*colCount)
    }
    
    subscript(i:Int,j:Int) ->Double {
        get {
            return values[i*colCount+j]
        }
        set(newValue) {
            values[i*colCount+j] = newValue
        }
    }
}
