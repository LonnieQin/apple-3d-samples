//
//  SpatialLine.swift
//  Math
//
//  Created by apple on 2016/10/13.
//  Copyright © 2016年 Lonnie. All rights reserved.
//

import UIKit

/// 空间直线
class SpatialLine: NSObject {
    
    /// 点
    var point:Double3
    
    /// 经过的向量
    var vector:Double3
    init(point:Double3,vector:Double3) {
        self.point = point
        self.vector = vector
        super.init()
    }
    
    func parameterFunxtion()->Parameter3DFunction {
        return Parameter3DFunction(x: point.x+vector.x*t, y: point.y+vector.y*t, z: point.z+vector.z*t)
    }
}
