//
//  Function.swift
//  Caculous
//
//  Created by apple on 2016/10/3.
//  Copyright © 2016年 Lonnie. All rights reserved.
//

import Foundation



/// 函数基类
public class Function: NSObject,NSCopying {
    public var multiplyer:Double = 1
    public var constant:Double = 0
    
    /// 子函数
    var children:[Function] {
        return []
    }
    
    /// 参数数量
    public var variableCount = 1
    /// 函数
    public var function:(Double)->Double
    /// 积分函数
    public var intergrateFunction:(Function)->Function = {_ in Number(0)}
    
    /// 微分函数
    public var diffenrentialFunction:(Function)->Function = {_ in Number(0)}
    
    /// 描述
    public var desc:String = ""
    
    /// 初始化方法
    ///
    /// - parameter function: 函数
    ///
    /// - returns: 函数对象
    public init(function:@escaping (Double)->Double) {
        self.function = function
        super.init()
    }
    
    public subscript(index: Double) -> Double {
        return multiplyer * function(index) + constant
    }
    override init() {
        self.function = {_ in 0}
        super.init()
    }
    
    /// 微分
    ///
    /// - returns: 函数对象
    public func  differential()->Function {
        return self.diffenrentialFunction(self)
    }
    
    public func differential(_ value:Double,_ delta:Double = 0.00000001)->Double {
        let y1:Double = self[value+delta]
        let y:Double = self[value]
        return (y1-y) / delta
    }
    
    /// 积分
    ///
    /// - returns: 函数对象
    public func  integrate()->Function {
        return self.intergrateFunction(self)
    }
    
    /// 积分
    ///
    /// - parameter range: 范围
    /// - parameter delta: 间隔
    ///
    /// - returns: <#return value description#>
    public func  integrate(range:Range<Double>,delta:Double = 0.00000001)->Double {
        var sum = 0.0
        var value = range.lowerBound
        while value < range.upperBound {
            sum += self[value] * delta
            value += delta
        }
        return sum
    }
    
    /// 描述
    override public var description: String {
        var string = ""
        if multiplyer == 1 {
            string += desc
        } else {
            string += "\(multiplyer)*\(desc)"
        }
        if constant > 0 {
            string += "\(constant)"
        }
        if constant < 0 {
            string += "-\(abs(constant))"
        }
        return string
    }
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let f = Function(function: self.function)
        f.multiplyer = self.multiplyer
        f.constant = self.constant
        f.intergrateFunction = self.intergrateFunction
        f.diffenrentialFunction = self.diffenrentialFunction
        f.desc = self.desc
        return f
    }
    
}


/// 赋值函数
public class AssignFunciton:Function {
    
    /// 等号左边的函数：只有打印作用
    public var leftFunction:Function
    
    /// 等号右边的函数
    public var rightFunction:Function
    
    override var children: [Function] {
        return [leftFunction,rightFunction]
    }
 
    /// 初始化
    ///
    /// - parameter leftFunction:  等号左边的函数
    /// - parameter rightFunction: 等号右边的函数
    ///
    /// - returns: <#return value description#>
    public init(leftFunction:Function,rightFunction:Function) {
        self.leftFunction = leftFunction
        self.rightFunction = rightFunction
        super.init(function: rightFunction.function)
    }

    public override var description: String {
        return "\(leftFunction)=\(rightFunction)"
    }
    
    /// 微分
    /// r'(t) = dr/dt = (df/dt)i+(dg/dt)j
    /// - returns: 函数对象
    public override func differential()->AssignFunciton {
        return  AssignFunciton(leftFunction:leftFunction,rightFunction:rightFunction.differential())
    }
    
    
    /// 积分
    ///
    /// - returns: 函数对象
    public override func integrate()->AssignFunciton {
        return  AssignFunciton(leftFunction:leftFunction,rightFunction:rightFunction.integrate())
    }
    public func copy(with zone: NSZone? = nil) -> AssignFunciton {
        let f = AssignFunciton(leftFunction: leftFunction, rightFunction: rightFunction)
        f.multiplyer = self.multiplyer
        f.constant = self.constant
        return f
    }
}


/// 参数方程
public class ParameterFunction:Function {
    open var xFunction:Function
    open var yFunction:Function
    public init(xFunction:Function,yFunction:Function) {
        self.xFunction = xFunction
        self.yFunction = yFunction
        super.init(function: {$0})
    }

    override var children: [Function] {
        return [xFunction,yFunction]
    }
    
    public func limit(at t:Double,dt:Double=0.0000001)->Vector2D {
        let dx = (self.xFunction[t+dt]-self.xFunction[t]) / dt
        let dy = (self.yFunction[t+dt]-self.yFunction[t]) / dt
        return Vector2D(x: dx, y: dy)
    }
    
    
    
    public override var description: String {
        return "r = \(xFunction)i+\(yFunction)j"
    }
    
    /// 微分
    /// r'(t) = dr/dt = (df/dt)i+(dg/dt)j 
    /// - returns: 函数对象
    public override func differential()->Function {
        return  ParameterFunction(xFunction:xFunction.differential(),yFunction:yFunction.differential())
    }
    
    
    /// 积分
    ///
    /// - returns: 函数对象
    public override func integrate()->Function {
        return  ParameterFunction(xFunction:xFunction.integrate(),yFunction:yFunction.integrate())
    }
    public func copy(with zone: NSZone? = nil) -> ParameterFunction {
        let f = ParameterFunction(xFunction: xFunction, yFunction: yFunction)
        f.multiplyer = self.multiplyer
        f.constant = self.constant
        return f
    }
}

public class PolerFunction:ParameterFunction {
    public init(_ function:Function) {
        super.init(xFunction: function*cos(t), yFunction: function*sin(t))
    }
}




public class FunctionIterator:IteratorProtocol {
    var Xs = [Double]()
    var Ys = [Double]()
    var Zs = [Double]()
    var currentIndex:Int = 0
    public func next() -> (Double,Double)? {
        if currentIndex < Xs.count {
            currentIndex += 1
            return (Xs[currentIndex-1],Ys[currentIndex-1])
        }
        
        return nil
    }
    public var underestimatedCount:Int {
        return Xs.count
    }
}

public func + (left:Function,right:Function)->Function {
    return PlusFunction(left, right)
}
public func + (left:ParameterFunction,right:ParameterFunction)->ParameterFunction {
    return ParameterFunction(xFunction: left.xFunction+right.xFunction, yFunction: left.yFunction+right.yFunction)
}
public func - (left:Function,right:Function)->Function {
    return MinusFunction(left,right)
}
public func * (left:Function,right:Function)->Function {
    return MultiplyFunction(left,right)
}

public func * (left:Double,right:Function)->Function {
    let f = right.copy() as! Function
    f.multiplyer *= left
    return f
}
public func * (left:Function,right:Double)->Function {
    let f = left.copy() as! Function
    f.multiplyer *= right
    return f
}
public func * (left:NumberFunction,right:Function)->Function {
    let f = right.copy() as! Function
    f.multiplyer *= left.number
    f.constant *= left.number
    return f
}
public func * (left:Function,right:NumberFunction)->Function {
    let f = left.copy() as! Function
    f.multiplyer *= right.number
    f.constant *= right.constant
    return f
}

public func + (left:Double,right:Function)->Function {
    let f = right.copy() as! Function
    f.constant += left
    return f
}
public func + (left:Function,right:Double)->Function {
    let f = left.copy() as! Function
    f.constant += right
    return f
}

public func - (left:Double,right:Function)->Function {
    let f = right.copy() as! Function
    f.constant -= left
    f.multiplyer *= -1
    return f
}
public func - (left:Function,right:Double)->Function {
    let f = left.copy() as! Function
    f.constant -= right
    return f
}


public func / (left:Function,right:Function)->Function {
    return DivideFunction(left,right)
}

/// 可嵌套函数
public class NestedFunction:Function {
    
    /// 外部函数
    var outsideFunction:Function
    
    /// 内部函数
    var insideFunction:Function
    override var children: [Function] {
        return [outsideFunction,insideFunction]
    }
    init(outsideFunction:Function,insideFunction:Function) {
        self.outsideFunction = outsideFunction
        self.insideFunction = insideFunction
        super.init()
        self.function = {self.outsideFunction[self.insideFunction[$0]]}
    }
    
    override public var description: String {
        var string = ""
        if multiplyer == 1 {
            string += "\(outsideFunction)(\(insideFunction))"
        } else {
            string += "\(multiplyer)*\(outsideFunction)(\(insideFunction))"
        }
        if constant > 0 {
            string += "+\(constant)"
        }
        if constant < 0 {
            string += "-\(abs(constant))"
        }
        return string
    }
    
    override public func differential() -> Function {
        let insideDif = insideFunction.differential()
        if  insideDif is NumberFunction{
            return NestedFunction(outsideFunction: outsideFunction.differential(), insideFunction: insideFunction)*(insideDif as! NumberFunction).number
        }
        return NestedFunction(outsideFunction: outsideFunction.differential(), insideFunction: insideFunction) * insideFunction.differential()
    }
    
    override public func copy(with zone: NSZone? = nil) -> Any {
        let f = NestedFunction(outsideFunction: outsideFunction, insideFunction: insideFunction)
        f.multiplyer = self.multiplyer
        f.constant = self.constant
        f.intergrateFunction = self.intergrateFunction
        f.diffenrentialFunction = self.diffenrentialFunction
        f.desc = self.desc
        return f
    }
}

///常量函数
public class NumberFunction:Function {
    var number:Double = 0
    init(number:Double) {
        self.number = number
        super.init(function: {_ in number})
    }
    override public var description: String {
        return "\(number)"
    }
    
    override public func integrate() -> Function {
        return x
    }
    
    override public func differential() -> Function {
        return Number(0)
    }
    
    override public func copy(with zone: NSZone? = nil) -> Any {
        return NumberFunction(number: number)
    }
    
}
public func Number(_ number:Double)->NumberFunction {
    return NumberFunction(number: number)
}

/// 变量函数 x,y,z
public class VariableFunction:Function {
    var name:String
    init(name:String) {
        self.name = name
        super.init(function: {$0})
        self.desc = name
    }
    
    override public func differential() -> Function {
        return Number(1*self.multiplyer)
    }
    override public func copy(with zone: NSZone? = nil) -> Any {
        let f = VariableFunction(name:name)
        f.constant = constant
        f.multiplyer = multiplyer
        return f
    }
}

//幂函数
public class PowerFunction:Function {
    var baseFunction:Function
    var index:Double = 1
    override var children: [Function] {
        return [baseFunction]
    }
    init(baseFunction:Function,index:Double = 1) {
        self.index =  index
        self.baseFunction = baseFunction
        super.init()
        self.set()
    }
    internal func set() {
        self.function = {
            return self.multiplyer * pow(self.baseFunction[$0], self.index)+self.constant
        }
    }
    
    public override func differential() -> Function {
        if self.index == 2 {
            return baseFunction*2
        }
        let f = PowerFunction(baseFunction: self.baseFunction,index:index-1)
        f.multiplyer = multiplyer * index
        return f * self.baseFunction.differential()
    }
    
    public override func integrate() -> Function {
        return PowerFunction(baseFunction: self.baseFunction,index:index+1)*(1/index+1)*multiplyer
    }
    /// 描述
    override public var description: String {
        var string = ""
        if multiplyer == 1 {
            string += "\(baseFunction)^\(index)"
        } else {
            string += "\(multiplyer)*\(baseFunction)^\(index)"
        }
        if constant > 0 {
            string += "+\(constant)"
        }
        if constant < 0 {
            string += "-\(abs(constant))"
        }
        return string
    }
    
    public override func copy() -> Any {
        let f = PowerFunction(baseFunction: baseFunction, index: index)
        f.multiplyer = multiplyer
        f.constant = constant
        return f
    }
}

/// 指数函数
public class ExponantialFunction:Function {
    var base:Double = 1
    var index:Function
    override var children: [Function] {
        return [index]
    }
    public init(base:Double,index:Function) {
        self.base = base
        self.index = index
        super.init()
        set()
    }
    
    internal func set() {
        self.function = {
            return self.multiplyer * pow(self.base, self.index[$0])+self.constant
        }
    }
    
    public override func differential() -> Function {
        if self.base == M_E {
            return self
        }
        let f = ExponantialFunction(base: self.base,index:index)
        f.multiplyer = multiplyer * log(self.base)
        return f
    }
    
    public override func integrate() -> Function {
        if self.base == M_E {
            return self
        }
        let f = ExponantialFunction(base: self.base,index:index)
        f.multiplyer = multiplyer * log(self.base)
        return f
    }
    /// 描述
    override public var description: String {
        var string = ""
        if multiplyer == 1 {
            string += "\(base)^\(index)"
        } else {
            string += "\(multiplyer)*\(base)^\(index)"
        }
        if constant > 0 {
            string += "+\(constant)"
        }
        if constant < 0 {
            string += "-\(abs(constant))"
        }
        return string
    }
    
    /// 复制
    ///
    /// - returns: 复制的对象
    public override func copy() -> Any {
        let f = ExponantialFunction(base: base, index: index)
        f.multiplyer = multiplyer
        f.constant = constant
        return f
    }
}




/// 操作符函数
public class BinaryFunction:Function {
    var function1:Function
    var function2:Function
    override var children: [Function] {
        return [function1,function2]
    }
    init(_ function1:Function,_ function2:Function) {
        self.function1 = function1
        self.function2 = function2
        super.init {_ in 0 }
    }
}


/// +
public class PlusFunction:BinaryFunction {
    override init(_ function1:Function,_ function2:Function) {
        super.init(function1,function2)
        self.function = {function1[$0]+function2[$0]}
    }
    
    override public var description: String {
        
        return "\(function1)+\(function2)"
    }
    
    override public func integrate() -> Function {
        return function1.integrate() + function2.integrate()
    }
    
    override public func differential() -> Function {
        return function1.differential() + function2.differential()
    }
}

/// -
public class MinusFunction:BinaryFunction {
    override init(_ function1:Function,_ function2:Function) {
        super.init(function1, function2)
        self.function = {function1[$0]-function2[$0]}
    }
    override public var description: String {
        return "\(function1)-\(function2)"
    }
    override public func integrate() -> Function {
        return function1.integrate() - function2.integrate()
    }
    
    override public func differential() -> Function {
        return function1.differential() - function2.differential()
    }
}


/// *
public class MultiplyFunction:BinaryFunction {
    override public init(_ function1:Function,_ function2:Function) {
        super.init(function1, function2)
        self.function = {
            return function1[$0]*function2[$0]
        }
    }
    override public var description: String {
        return "(\(function1)*\(function2))"
    }
    
    override public func differential() -> Function {
        return function1.differential() * function2 + function1 * function2.differential()
    }
}


/// 除法函数
public class DivideFunction:BinaryFunction {
    override public  init(_ function1:Function,_ function2:Function) {
        super.init(function1,function2)
        self.function = {function1[$0]/function2[$0]}
    }
     override public var description: String {
        return "(\(function1))÷(\(function2))"
    }
    override public func differential() -> Function {
        return DivideFunction(MinusFunction(MultiplyFunction(function2,function1.differential()),MultiplyFunction(function1,function2.differential())), MultiplyFunction(function2,function2))
    }
}

public class Parameter3DFunction:Function {
    var xFunction:Function
    var yFunction:Function
    var zFunction:Function
    init(x:Function,y:Function,z:Function = Number(0)) {
        self.xFunction = x
        self.yFunction = y
        self.zFunction = z
        super.init()
    }
    init(polarFunction:PolerFunction,z:Function = Number(0)) {
        self.xFunction = polarFunction.xFunction
        self.yFunction = polarFunction.yFunction
        self.zFunction = z
        super.init()
    }
}
