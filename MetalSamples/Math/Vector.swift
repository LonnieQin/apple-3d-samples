//
//  Vector.swift
//  Math
//
//  Created by apple on 2016/10/5.
//  Copyright © 2016年 Lonnie. All rights reserved.
//

import UIKit

//MARK:9 平面向量
public struct Vector2D{
    public var x:Double = 0
    public var y:Double = 0
    public init(x:Double,y:Double) {
        self.x = x
        self.y = y
    }
    /// 分量形式
    ///
    /// - returns: 返回向量的分量形式
    public func componentWise()->String {
        return "<\(x),\(y)>"
    }
    
    public func standardUnit()->String {
        return "(\(x)i+\(y)j)"
    }
    
    //|v|
    public func length()->Double {
        return sqrt(pow(x,2)+pow(y, 2))
    }
    
    //单位向量
    public func unit()->Vector2D{
        return self / length()
    }
    
    
    /// 求出切线的单位向量
    ///
    /// - parameter function: 函数
    ///
    /// - returns: 切线的单位向量
    public func tangentVector(function:(Double)->Double)->Vector2D
    {
        return Vector2D(x: 1, y: differential(function: function, x: x)).unit()
    }
    
    public func normalVector(function:(Double)->Double)->Vector2D {
        return Vector2D(x: 1, y: -differential(function: function, x: x)).unit()
    }
}

//两个向量相加
public func + (left:Vector2D,right:Vector2D)->Vector2D {
    return Vector2D(x: left.x+right.x, y: left.y+right.y)
}
//两个向量相减
public func - (left:Vector2D,right:Vector2D)->Vector2D {
    return Vector2D(x: left.x-right.x, y: left.y-right.y)
}

///两个向量相乘
public func * (left:Vector2D,right:Vector2D)->Double  {
    return left.x*right.x+left.y*right.y
}

infix operator ^^
public func ^^ (left:Double,right:Double)->Double {
    return pow(left, right)
}

public func * (left:Vector2D,right:Double)->Vector2D {
    return Vector2D(x: left.x*right, y: left.y*right)
}
public func * (left:Double,right:Vector2D)->Vector2D {
    return Vector2D(x: left*right.x, y: left*right.y)
}
public func / (left:Vector2D,right:Double)->Vector2D {
    return Vector2D(x: left.x/right, y: left.y/right)
}


//两个向量的夹角
public func acos(_ vector1:Vector2D,_ vector2:Vector2D )->Double {
    return  acos(vector1*vector2 / (vector1.length()*vector2.length()))
}

///u在v上面的投影
public func proj(_ u:Vector2D,_ v:Vector2D)->Vector2D {
    return v * (u*v/pow(v.length(), 2))
}

///u在v的正交向量
public func orthogonal(_ u:Vector2D,_ v:Vector2D)->Vector2D {
    return u - proj(u, v)
}

public func orthogonalSum(_ u:Vector2D,_ v:Vector2D) {
    print("\(u)=\(proj(u,v))+\(orthogonal(u, v))")
}

///u在v上的数值分量
public func numericComponents(_ u:Vector2D,_ v:Vector2D)->Double {
    return u * v / v.length()
}

extension Function {
    //y = k*x+b b = y-k*x
    public class func vectorFunction(vector:Vector2D,point:CGPoint = CGPoint.zero)->Function {
        let k = vector.y / vector.x
        let b = Double(point.y) - k*Double(point.x)
        return k*x + b
    }
    public class func vectorFunction(point1:CGPoint,point2:CGPoint)->Function {
        let k = Double((point2.y-point1.y) / (point2.x-point1.x))
        let b = Double(point1.y) - k*Double(point1.x)
        return k*x + b
    }
}

