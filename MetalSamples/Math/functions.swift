//
//  functions.swift
//  ThreeDSamples
//
//  Created by lonnie on 2017/3/1.
//  Copyright © 2017年 Apple. All rights reserved.
//

import Foundation
fileprivate let CGFloatE = CGFloat(M_E)
func ch(_ x:CGFloat)->CGFloat {
    return (pow(CGFloatE, x)+pow(CGFloatE,-x)) * 0.5
}
func sh(_ x:CGFloat)->CGFloat {
    return (pow(CGFloatE, x)+pow(CGFloatE,-x)) / 2
}
let PI = CGFloat(M_PI)

func crossProduct(a:SCNVector3,b:SCNVector3)->SCNVector3
{
    return SCNVector3Make(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x)
}

/// 变成单位向量
///
/// - Parameter v: 当前向量
/// - Returns: 单位向量
func normalize(_ v:SCNVector3)->SCNVector3
{
    let len = sqrt(pow(v.x, 2) + pow(v.y, 2) + pow(v.z, 2))
    return SCNVector3Make(v.x/len, v.y/len, v.z/len)
}

/// 计算积分
///
/// - parameter from:     下界
/// - parameter to:       上界
/// - parameter delta:    间隔
/// - parameter function: F(X)
///
/// - returns: <#return value description#>
public func intergrate(from:Double,to:Double,delta:Double,function:(Double)->Double)->Double {
    var sum = 0.0
    var value = from
    while value < to {
        sum += function(value) * delta
        value += delta
    }
    return sum
}


/// 计算微分
///
/// - Parameters:
///   - function: 函数
///   - x: x的值
///   - delta: 步长
/// - Returns: 微分值
public func  differential(function:(Double)->Double,x:Double,delta:Double = 0.00000000001)->Double {
    let y1:Double = function(x+delta)
    let y:Double = function(x)
    return (y1-y) / delta
}

/// 产生SIN函数
///
/// - parameter fun: 函数
///
/// - returns: SIN函数
public func sin(_ fun:Function)->Function {
    let f = Function(function: sin)
    f.desc = "sin"
    f.diffenrentialFunction = {f in
        return f.multiplyer*cos(fun)
    }
    f.intergrateFunction = {f in
        return -f.multiplyer*cos(fun)
    }
    return NestedFunction(outsideFunction: f, insideFunction: fun)
}

/// 产生cos的函数
///
/// - parameter fun: 函数
///
/// - returns: cos函数
public func cos(_ fun:Function)->Function {
    let f = Function(function: cos)
    f.desc = "cos"
    f.diffenrentialFunction = {f in
        return -f.multiplyer*sin(fun)
    }
    f.intergrateFunction = {f in
        return f.multiplyer*sin(fun)
    }
    return NestedFunction(outsideFunction: f, insideFunction: fun)
}


/// 产生tan的函数
///
/// - parameter fun: 函数
///
/// - returns: tan函数
public func tan(_ fun:Function)->Function {
    let f = Function(function: tan)
    f.desc = "tan"
    return NestedFunction(outsideFunction: f, insideFunction: fun)
}


/// 产生log的函数
///
/// - parameter fun: <#fun description#>
///
/// - returns: <#return value description#>
public func log(_ fun:Function)->Function {
    let f = Function(function: log)
    f.desc = "log"
    f.diffenrentialFunction = {f in
        return Number(1) / fun
    }
    f.intergrateFunction = {f in
        return PlusFunction(MinusFunction(MultiplyFunction(fun,log(fun)), fun), c)
    }
    return NestedFunction(outsideFunction: f, insideFunction: fun)
}

public func abs(_ fun:Function)->Function {
    return Function(function: {return abs(fun[$0])})
}


public func pow(_ base:Function,_ index:Double)->PowerFunction {
    return PowerFunction(baseFunction: base, index: index)
}
public func pow(_ base:Double,_ index:Function)->ExponantialFunction {
    return ExponantialFunction(base: base, index: index)
}


///表示数学的x、y、z等符号
public let X = VariableFunction(name: "X")
public let Y = VariableFunction(name: "Y")
public let Z = VariableFunction(name: "Z")
public let x = VariableFunction(name: "x")
public let t = VariableFunction(name: "t")
public let y = VariableFunction(name: "y")
public let z = VariableFunction(name: "z")
public let c = VariableFunction(name: "c")
public let 𝝷 = VariableFunction(name: "𝝷")
public let 𝝿 = VariableFunction(name: "𝝿")

public let i = VariableFunction(name: "i")
public let j = VariableFunction(name: "k")
public let k = VariableFunction(name: "j")
