//
//  Projectile.swift
//  Math
//
//  Created by apple on 2016/10/7.
//  Copyright © 2016年 Lonnie. All rights reserved.
//

import UIKit


/// 抛射体
public struct Projectile {
    public var v0:Double
    public var angle:Double
    public var position0:CGPoint
    public var resistance:ParameterFunction
    public init(v0:Double,angle:Double,position0:CGPoint = CGPoint.zero,resistance:ParameterFunction =  ParameterFunction(xFunction: Number(0), yFunction: Number(0))) {
        self.v0 = v0
        self.angle = angle
        self.position0 = position0
        self.resistance = resistance
    }
    public func function()->ParameterFunction {
        return ParameterFunction(xFunction: Double(position0.x)+v0*cos(angle)*t, yFunction: Double(position0.y)+v0*sin(angle)*t-4.9*t*t)
    }
    
    public func maxHeight()->Double {
        return pow(v0*sin(angle),2)/(2*9.8)+Double(position0.y)
    }
    public func flyTime()->Double {
        return 2 * v0 * sin(angle) / 9.8
    }
    
    /// 射程
    ///
    /// - returns: 射程
    public func rangeOfFire()->Double {
        return v0*v0/9.8 * sin(2*angle)
    }
}
