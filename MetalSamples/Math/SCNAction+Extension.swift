//
//  SCNAction+Extension.swift
//  ThreeDSamples
//
//  Created by lonnie on 2017/3/9.
//  Copyright © 2017年 Apple. All rights reserved.
//
import SceneKit
extension SCNAction {
    class func moveBy(function:Parameter3DFunction,duration:Double = 100000000,relateToCurrentPosition:Bool = true)->SCNAction{
        var ft = true
        var p = SCNVector3Zero
        return SCNAction.customAction(duration: duration, action: {node,time in
            if ft {
                ft = false
                p = node.position
            }
            let t = Double(time)
            if relateToCurrentPosition {
                node.position = SCNVector3(function.xFunction[t]+Double(p.x), function.yFunction[t]+Double(p.y), function.zFunction[t]+Double(p.z))
                
            } else {
                node.position = SCNVector3(function.xFunction[t], function.yFunction[t], function.zFunction[t])
                
            }
            
        })
    }
}
