//
//  Vector3D.swift
//  Math
//
//  Created by apple on 2016/10/9.
//  Copyright © 2016年 Lonnie. All rights reserved.
//

import UIKit
public struct Double3 {
    var x:Double
    var y:Double
    var z:Double
    init(_ x:Double,_ y:Double,_ z:Double) {
        self.x = x
        self.y = y
        self.z = z
    }
}

public class Determinant3By3:NSObject{
    var u:Double3
    var v:Double3
    private var numbers:[[Double]] = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
    init(u:Double3,v:Double3) {
        self.u = u
        self.v = v
        numbers = [[1,1,1],[self.u.x,self.u.y,self.u.z],[self.v.x,self.v.y,self.v.z]]
    }
    
    override public var description:String {
        return "|i,j,k|\n|\n\(u.x),\(u.y),\(u.z)|\n\(v.x),\(v.y),\(v.z)|||"
    }
}

public

extension Double3 {
    //0向量
    static let zero = Double3(0, 0, 0)
    //i向量
    static let i = Double3(1, 0, 0)
    //i向量
    static let j = Double3(0, 1, 0)
    //i向量
    static let k = Double3(0, 0, 1)
}

extension Double3 {
    
    ///长度
    var length:Double {
        return sqrt(x*x+y*y+z*z)
    }
    
    
    /// 和另外一个点的距离
    ///
    /// - parameter value: 另一个点
    ///
    /// - returns: 距离
    func distTo(value:Double3)->Double {
        return sqrt(pow(x-value.x,2)+pow(y-value.y,2)+pow(z-value.z,2))
    }
    
    
    /// 和另一个向量的中点
    ///
    /// - parameter value: 值
    ///
    /// - returns: 中点
    func middlePoint(between value:Double3)->Double3 {
        return Double3((x+value.x)/2,(y+value.y)/2,(z+value.z)/2)
    }
    
    /// 在v上的投影
    /// u在v上的向量投影：projvu = (u.v/|v|^2)v
    ///
    /// - parameter v: 向量
    /// - returns: 投影
    func proj(at v:Double3)->Double3 {
        return (self*v / pow(v.length, 2))*v
    }
    
    
    /// 在v上的数量分量
    /// u在v上的数量分量= |u|cos(theta) = u*v/|v|^2 = u*v/|v|
    ///
    /// - parameter v: 向量
    ///
    /// - returns: 数量分量
    func quantityComponent(at v:Double3)->Double {
        return self * v / v.length
    }
    
    
}

//两个向量相加
public func + (left:Double3,right:Double3)->Double3 {
    return Double3(left.x+right.x,left.y+right.y,left.z+right.z)
}
//两个向量相减
public func - (left:Double3,right:Double3)->Double3 {
    return Double3(left.x-right.x,left.y-right.y,left.z-right.z)
}


/// 向量乘以数字
///
/// - parameter left:  向量
/// - parameter right: 数字
///
/// - returns: 向量
public func * (left:Double3,right:Double)->Double3 {
    return Double3(left.x*right,left.y*right,left.z*right)
}
public func * (right:Double,left:Double3)->Double3 {
    return Double3(left.x*right,left.y*right,left.z*right)
}

/// 向量除以数字
///
/// - parameter left:  向量
/// - parameter right: 数字
///
/// - returns: 向量
public func / (left:Double3,right:Double)->Double3 {
    return Double3(left.x/right,left.y/right,left.z/right)
}

/// 两个三维向量的点积
/// 点积的计算公式:u.v = u1v1+u2v2+u3v3.
///
/// - parameter left:  第一个向量
/// - parameter right: 第二个向量
///
/// - returns: 点积
public func * (left:Double3,right:Double3)->Double {
    return  left.x*right.x+left.y*right.y+left.z*right.z
}
infix operator × { associativity left precedence 150 }

/// u×v返回一个行列式
///
/// - parameter left:  左边的向量
/// - parameter right: 右边的向量
///
/// - returns: <#return value description#>
public func × (left:Double3,right:Double3)->Determinant3By3 {
    return Determinant3By3(u: left, v: right)
}
/// 两个非零向量的夹角
/// 两个非零向量的夹角为 theta = cos-1(u.v/(|u||v|))
///
/// - parameter left:  第一个向量
/// - parameter right: 第二个向量
///
/// - returns: 两个三维向量的夹角
func acos(left:Double3,right:Double3)->Double {
    return acos(left*right/(left.length*right.length))
}

