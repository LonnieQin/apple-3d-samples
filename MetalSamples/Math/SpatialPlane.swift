//
//  SpatialPlane.swift
//  Math
//
//  Created by apple on 2016/10/13.
//  Copyright © 2016年 Lonnie. All rights reserved.
//

import UIKit

class SpatialPlane: NSObject {
    var point:Double3
    var verticalVector:Double3
    init(point:Double3,verticalVector:Double3) {
        self.point = point
        self.verticalVector = verticalVector
        super.init()
    }
    
    /// 过点P0(x0,y0,z0)且成垂直n=Ai+Bj+Ck的平面方程为A(x-x0)+B(y-y0)+C(z-z0) = 0
    func desc()->String {
        return "\(verticalVector.x)(x-\(point.x))+\(verticalVector.y)(y-\(point.y))+\(verticalVector.z)(z-\(point.z))=0"
    }
}
