//
//  MetalKitSection.swift
//  ThreeDSamples
//
//  Created by Apple on 2017/3/7.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit
import MetalKit
import Metal
func MetalKitSection()->Section {
    let s = Section(title:"Metal")
    s.addRow(title:"Triangle") {c in
        let controller = UIViewController()
        controller.view = LNMetalView(frame: .zero, device: MTLCreateSystemDefaultDevice())
        
        c.navigationController?.pushViewController(controller, animated: true)
    }
    /*
    s.addRow(title:"Triangle2") {c in
        let controller = UIViewController()
        controller.view = LN2MetalView(frame: .zero)
        c.navigationController?.pushViewController(controller, animated: true)
    }
    */
    s.addRow(title: "Rotating Cube") { (c) in
        c.navigationController?.pushViewController(RCViewController(), animated: true)
    }
    s.addRow(title: "Cubes") { (c) in
        c.navigationController?.pushViewController(CubesViewController(), animated: true)
    }
    return s
}
