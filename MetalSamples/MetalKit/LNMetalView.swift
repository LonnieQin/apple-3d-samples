//
//  LNMetalView.swift
//  ThreeDSamples
//
//  Created by Apple on 2017/3/7.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit

class LNMetalView: MTKView {
    struct Parameter { 
        var matrix:float4x4 = float4x4()
    }
    struct Vertex {
        var position:vector_float4
        var color:vector_float4
    }
    
    struct Matrix {
        var m:[Float]
        init() {
            m = [1, 0, 0, 0,
                 0, 1, 0, 0,
                 0, 0, 1, 0,
                 0, 0, 0, 1]
        }
        
        mutating func translate(position:float3) {
            m[12] = position.x
            m[13] = position.y
            m[14] = position.z
        }
        
        mutating func scale(scale:float3) {
            m[0] = scale.x
            m[5] = scale.y
            m[10] = scale.z
            m[15] = 1
        }
        
        mutating func rotate(rot:float3) {
            m[0] = cos(rot.y) * cos(rot.z)
            m[4] = cos(rot.z) * sin(rot.x) * sin(rot.y) - cos(rot.x) * sin(rot.z)
            m[8] = cos(rot.x) * cos(rot.z) * sin(rot.y) + sin(rot.x) * sin(rot.z)
            m[1] = cos(rot.y) * sin(rot.z)
            m[5] = cos(rot.x) * cos(rot.z) + sin(rot.x) * sin(rot.y) * sin(rot.z)
            m[9] = -cos(rot.z) * sin(rot.x) + cos(rot.x) * sin(rot.y) * sin(rot.z)
            m[2] = -sin(rot.y)
            m[6] = cos(rot.y) * sin(rot.x)
            m[10] = cos(rot.x) * cos(rot.y)
            m[15] = 1.0
        }
        
    }
    
    var commandQueue:MTLCommandQueue?
    var rps:MTLRenderPipelineState?
    var vertexData:[Vertex]?
    var vertexBuffer:MTLBuffer?
    var parameter = Parameter.init()
    override init(frame frameRect: CGRect, device: MTLDevice?) {
        super.init(frame: frameRect, device: device)
        render() 
        var matrix = Matrix()
        matrix.rotate(rot: float3.init(0, 0,0.1))
        matrix.scale(scale: float3.init(0.25, 0.25, 0.25))
        matrix.translate(position: float3.init(0, 0.5, 0))
        print(matrix)
        parameter.matrix = float4x4(rows: [.init(matrix.m[0], matrix.m[1], matrix.m[2], matrix.m[3]),
                                                .init(matrix.m[4], matrix.m[5], matrix.m[6], matrix.m[7]),
                                                .init(matrix.m[8], matrix.m[9], matrix.m[10], matrix.m[11]),
                                                .init(matrix.m[12], matrix.m[13], matrix.m[14], matrix.m[15])])
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func render() {
        commandQueue = device!.makeCommandQueue()
        vertexData = [Vertex(position: [-1.0,-1.0,1.0,1.0], color: [1,0,0,1]),
                      Vertex(position: [1.0,-1.0,0.0,1.0], color: [0,1,0,1]),
                      Vertex(position: [0,1,0.0,1.0], color: [0,0,1,1])]
        let dataSize = vertexData!.count * MemoryLayout<Vertex>.size
        vertexBuffer = device?.makeBuffer(bytes: vertexData!, length: dataSize, options: [])
        let library = device!.newDefaultLibrary()!
        let vertex_func = library.makeFunction(name: "vertex_f")
        let frag_func = library.makeFunction(name: "frag_f")
        let rpid = MTLRenderPipelineDescriptor()
        rpid.vertexFunction = vertex_func
        rpid.fragmentFunction = frag_func
        rpid.colorAttachments[0].pixelFormat = .bgra8Unorm
        do {
            try rps = device!.makeRenderPipelineState(descriptor: rpid)
        } catch let error {
            print("Error:\(error)")
        }
    }
    override func draw(_ rect: CGRect) {
        if let drawable = currentDrawable,let rpd = currentRenderPassDescriptor {
            rpd.colorAttachments[0].texture = currentDrawable!.texture
            rpd.colorAttachments[0].clearColor = MTLClearColorMake(0, 0.5, 0.5, 1)
            rpd.colorAttachments[0].loadAction = .clear
            let buffer = device!.makeCommandQueue().makeCommandBuffer()
            let encoder = buffer.makeRenderCommandEncoder(descriptor: rpd)
            encoder.setRenderPipelineState(rps!)
            encoder.setVertexBuffer(vertexBuffer, offset: 0, at: 0)
            encoder.setVertexBytes(&parameter, length: MemoryLayout<Parameter>.size, at: 1)
            encoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: 3, instanceCount: 1)
            encoder.endEncoding()
            buffer.present(drawable)
            buffer.commit()
        }
    }
}
