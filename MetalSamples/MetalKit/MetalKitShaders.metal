//
//  MetalKitShaders.metal
//  ThreeDSamples
//
//  Created by Apple on 2017/3/7.
//  Copyright © 2017年 Apple. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;
struct Parameter {
    float4x4 matrix;
};
struct Vertex {
    float4 position [[position]];
    float4 color;
};

vertex Vertex vertex_f(constant Vertex * vertices [[buffer(0)]],
                       constant Parameter &parameter [[buffer(1)]],
                       uint vid [[vertex_id]]) {
    Vertex po = vertices[vid];
    po.position = parameter.matrix * float4(po.position);
    return po;
}

fragment float4 frag_f(Vertex vert [[stage_in]]) {
    return vert.color;
}
