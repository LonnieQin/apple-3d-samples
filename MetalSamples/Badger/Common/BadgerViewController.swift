//
//  BadgerViewController.swift
//  MetalSamples
//
//  Created by lonnie on 2017/2/21.
//  Copyright © 2017年 Apple. All rights reserved.
//

import UIKit
import SceneKit
import GameKit

class BadgerViewController: UIViewController,SCNSceneRendererDelegate {
    
    @IBOutlet var sceneView: BadgerView!
    struct Assets {
        static let basePath = "badger.scnassets/"
        private static let soundsPath = basePath + "sounds/"
        static func sound(named name:String)->SCNAudioSource {
            guard let source = SCNAudioSource(named: soundsPath + name) else {
                fatalError()
            }
            return source
        }
        
        static func animation(named name:String)->CAAnimation {
            guard let scene = SCNScene(named: basePath+name) else {
                fatalError()
            }
            var animation:CAAnimation?
            scene.rootNode.enumerateChildNodes { (node, stop) in
                guard let firstKey = node.animationKeys.first else {return}
                animation = node.animation(forKey: firstKey)
                stop.initialize(to: true)
            }
            guard let foundAnimation = animation else {
                fatalError()
            }
            
            foundAnimation.fadeInDuration = 0.3
            foundAnimation.fadeOutDuration = 0.3
            foundAnimation.repeatCount = 1
            return foundAnimation
        }
        
        static func scene(named name:String)->SCNScene {
            guard let scene = SCNScene(named: basePath + name) else {
                fatalError()
            }
            return scene
        }
    }
    
    struct Trigger {
        let position:float3
        let action:(BadgerViewController)->()
    }
    
    private enum CollectableState:UInt {
        case notCollected = 0
        case beingCollected = 2
    }
    
    private enum GameState:UInt {
        case notStarted = 0
        case started = 1
    }
    
    /// Determines if the level uses local sun.
    let isUsingLocalSun = true
    
    ///Determines if audio should be enabled.
    let isSoundEnabled = true
    
    let speedFactor:Float = 1.5
    
    //MARK:Scene Properties
    let scene = Assets.scene(named: "scene.scn")
    
    // MARK:Animation Properties
    let character:SCNNode
    let idleAnimationOwner:SCNNode
    let cartAnimationName:String
    
    
    let jumpAnimation = Assets.animation(named: "animation-jump.scn")
    let squatAnimation = Assets.animation(named: "animation-squat.scn")
    let leanLeftAnimation = Assets.animation(named: "animation-lean-left.scn")
    let leanRightAnimation = Assets.animation(named: "animation-lean-right.scn")
    let slapAnimation = Assets.animation(named: "animation-slap.scn")
    
    let leftHand:SCNNode
    let rightHand:SCNNode
    
    var sunTargetRelativeToCamera:SCNVector3
    var sunDirection:SCNVector3
    var sun:SCNNode
    
    // Sparkles effect
    var sparkles:SCNParticleSystem
    var stars:SCNParticleSystem
    var leftWheelEmitter:SCNNode
    var rightWheelEmitter:SCNNode
    var headEmitter:SCNNode
    var wheels:SCNNode
    
    //Collect particles
    var collectParticleSystem:SCNParticleSystem
    var collectBigParticleSystem:SCNParticleSystem
    
    // State
    var squatCounter = 0
    var isOverWood = false
    
    // MARK:Sound Properties
    var railSoundSpeed:UInt = 0
    var hitSound = Assets.sound(named: "hit.mp3")
    var railHighSpeedSound = Assets.sound(named: "rail_highspeed_loop.mp3")
    var railMediumSpeedSound = Assets.sound(named: "rail_normalspeed_loop.mp3")
    var railLowSpeedSound = Assets.sound(named: "rail_slowspeed_loop.mp3")
    var railWoodSound = Assets.sound(named: "rail_wood_loop.mp3")
    var railSqueakSound = Assets.sound(named: "cart_turn_squeak.mp3")
    var cartHide = Assets.sound(named: "cart_hide.mp3")
    var cartJump = Assets.sound(named: "cart_jump.mp3")
    var cartTurnLeft = Assets.sound(named: "cart_turn_left.mp3")
    var cartTurnRight = Assets.sound(named: "cart_turn_right.mp3")
    var cartBoost = Assets.sound(named: "cart_boost.mp3")
    
    //MARK:Collectable Properties
    
    let collectables:SCNNode
    let speedItems:SCNNode
    let collectSound = Assets.sound(named: "collect1.mp3")
    let collectSound2 = Assets.sound(named: "collect2.mp3")
    
    //MARK:Triggers
    var triggers = [Trigger]()
    var activeTriggerIndex = -1
    static func instance()->BadgerViewController {
        return UIStoryboard(name: "BadgerViewController", bundle: nil).instantiateInitialViewController() as! BadgerViewController
    }
    private var gameState:GameState = .notStarted

    required init?(coder aDecoder: NSCoder) {
        character = scene.rootNode.childNode(withName: "Bob_root", recursively: true)!
        let idleScene = Assets.scene(named: "animation-idle.scn")
        let characterHierarchy = idleScene.rootNode.childNode(withName: "Bob_root", recursively: true)!
        for node in characterHierarchy.childNodes {
            character.addChildNode(node)
        }
        idleAnimationOwner = character.childNode(withName: "Dummy_kart_root", recursively: true)!
        cartAnimationName = scene.rootNode.animationKeys.first!
        let idleAnimation = Assets.animation(named: "animation-start-idle.scn")
        idleAnimation.repeatCount = Float.infinity
        character.addAnimation(idleAnimation, forKey: "start")
        let sparkleScene = Assets.scene(named: "sparkles.scn")
        let sparkleNode = sparkleScene.rootNode.childNode(withName: "sparkles", recursively: true)!
        sparkles = sparkleNode.particleSystems![0]
        sparkles.loops = false
        let starsNode = sparkleScene.rootNode.childNode(withName: "slap", recursively: true)!
        stars = starsNode.particleSystems![0]
        stars.loops = false
        
        collectParticleSystem = SCNParticleSystem(named: "collect.scnp", inDirectory: "badger.scnassets")!
        collectParticleSystem.loops = false
        collectBigParticleSystem = SCNParticleSystem(named: "collect-big.scnp", inDirectory: "badger.scnassets")!
        collectBigParticleSystem.loops = false
        leftHand = character.childNode(withName: "Bip001_L_Finger0Nub", recursively: true)!
        rightHand = character.childNode(withName: "Bip001_R_Finger0Nub", recursively: true)!
        leftWheelEmitter = character.childNode(withName: "Dummy_leftWheel_sparks", recursively: true)!
        rightWheelEmitter = character.childNode(withName: "Dummy_rightWheel_sparks", recursively: true)!
        wheels = character.childNode(withName: "wheels_front", recursively: true)!
        
        headEmitter = SCNNode()
        headEmitter.position = SCNVector3Make(0, 1, 0)
        character.addChildNode(headEmitter)
        
        let wheelAnimation = CABasicAnimation(keyPath: "eulerAngles.x")
        wheelAnimation.byValue = 10.0
        wheelAnimation.duration = 1.0
        wheelAnimation.repeatCount = Float.infinity
        wheelAnimation.isCumulative = true
        wheels.addAnimation(wheelAnimation, forKey: "wheel")
        slapAnimation.fadeInDuration = 0.0
        
        collectables = scene.rootNode.childNode(withName: "Collectables", recursively: false)!
        speedItems = scene.rootNode.childNode(withName: "SpeedItems", recursively: false)!
        
        collectSound.volume = 5.0
        collectSound2.volume = 5.0
        let sounds = [
            railSqueakSound,collectSound,collectSound2,
            hitSound,railHighSpeedSound,railMediumSpeedSound,
            railLowSpeedSound,railWoodSound,railSqueakSound,
            cartHide,cartJump,cartTurnLeft,cartTurnRight
        ]
        
        for sound in sounds {
            sound.isPositional = false
            sound.load()
        }
        
        if isUsingLocalSun {
            sun = scene.rootNode.childNode(withName: "Direct001", recursively: false)!
            sun.light?.shadowMapSize = CGSize(width: 2048, height: 2048)
            sun.light?.orthographicScale = 10
            sunTargetRelativeToCamera = SCNVector3Make(0, 0, -10)
            sun.position = SCNVector3Zero
            sunDirection = sun.convertPosition(SCNVector3.init(0, 0, -1), to: nil)
        } else {
            sun = SCNNode()
            sunTargetRelativeToCamera = SCNVector3Zero
            sunDirection = SCNVector3Zero
        }
       super.init(coder: aDecoder)
    }
    
    func configureScene() {
        let leftEvent1 = SCNAnimationEvent(keyTime: 0.15) {[unowned self] (a,b,c) in
            self.leftWheelEmitter.addParticleSystem(self.sparkles)
        }
        let leftEvent2 = SCNAnimationEvent(keyTime: 0.9) {[unowned self] (a,b,c) in
            self.rightWheelEmitter.addParticleSystem(self.sparkles)
        }
        let rightEvent1 = SCNAnimationEvent(keyTime: 0.9) {[unowned self] (a,b,c) in
            self.leftWheelEmitter.addParticleSystem(self.sparkles)
        }
        
        leanLeftAnimation.animationEvents = [leftEvent1,leftEvent2]
        leanRightAnimation.animationEvents = [rightEvent1]
        let sceneView = self.view as! SCNView
        sceneView.antialiasingMode = .none
        
        let triggerGroup = scene.rootNode.childNode(withName: "triggers", recursively: false)
        triggers = triggerGroup!.childNodes.flatMap({ (node) -> Trigger? in
            let triggerName = node.name! as NSString
            let triggerPosition = float3(node.position)
            if triggerName.hasPrefix("Trigger_speed") {
                let speedValueOffset = "Trigger-speedX_".characters.count
                let speedValue = triggerName.substring(from: speedValueOffset).replacingOccurrences(of: "_", with: ".")
                return Trigger(position: triggerPosition, action: { (controller) in
                    controller.trigger(characterSpeed:Float(speedValue) ?? 0)
                })
            }
            
            if triggerName.hasPrefix("Trigger_obstacle") {
                return Trigger(position: triggerPosition, action: { (controller) in
                    controller.sceneView.didDamageLife()
                    controller.triggerCollision()
                })
            }
            if triggerName.hasPrefix("Trigger_turn_start")  {
                return Trigger(position: triggerPosition, action: { (controller) in
                    controller.startTurn()
                })
            }
            if triggerName.hasPrefix("Trigger_turn_stop")  {
                return Trigger(position: triggerPosition, action: { (controller) in
                    controller.stopTurn()
                })
            }
            if triggerName.hasPrefix("Trigger_wood_start")  {
                return Trigger(position: triggerPosition, action: { (controller) in
                    controller.startWood()
                })
            }
            if triggerName.hasPrefix("Trigger_wood_stop")  {
                return Trigger(position: triggerPosition, action: { (controller) in
                    controller.stopWood()
                })
            }
            if triggerName.hasPrefix("Trigger_highSpeed")  {
                return Trigger(position: triggerPosition, action: { (controller) in
                    controller.changeSpeedSound(speed: 3)
                })
            }
            if triggerName.hasPrefix("Trigger_normalSpeed")  {
                return Trigger(position: triggerPosition, action: { (controller) in
                    controller.changeSpeedSound(speed: 2)
                })
            }
            if triggerName.hasPrefix("Trigger_slowSpeed")  {
                return Trigger(position: triggerPosition, action: { (controller) in
                    controller.changeSpeedSound(speed: 1)
                })
            }
            return nil
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureScene()
        let sceneView = self.view as! SCNView
        sceneView.scene = scene
        
        let cartAnimation = scene.rootNode.animation(forKey: cartAnimationName)!
        cartAnimation.animationEvents = [SCNAnimationEvent.init(keyTime: 0.9, block: {[unowned self] a,b,c in
            self.respawnCollectables()
        })]
        scene.rootNode.addAnimation(cartAnimation, forKey: cartAnimationName)
        sceneView.prepare(scene, shouldAbortBlock: nil)
        sceneView.delegate = self
        sceneView.pointOfView = sceneView.scene?.rootNode.childNode(withName: "camera_depart", recursively: true)
        
        let sound = Assets.sound(named: "wind.m4a")
        sound.loops = true
        sound.isPositional = false
        sound.shouldStream = true
        sound.volume = 8.0
        sceneView.scene?.rootNode.addAudioPlayer(SCNAudioPlayer(source: sound))
        sceneView.contentScaleFactor = 1.3
        characterSpeed = 0.0
        setupGameControllers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK:Render LOOP
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        activateTriggers()
        collectItems()
        if isUsingLocalSun {
            let target = renderer.pointOfView?.presentation.convertPosition(sunTargetRelativeToCamera, to: nil)
            sun.position = SCNVector3(float3(target!) - float3(sunDirection) * 10.0)
        }
    }
    
    func startTurn() {
        guard isSoundEnabled else {return}
        let player = SCNAudioPlayer(source: railSqueakSound)
        leftWheelEmitter.addAudioPlayer(player)
    }
    
    func stopTurn() {
        guard isSoundEnabled else {return}
        leftWheelEmitter.removeAllAudioPlayers()
    }
    
    func startWood() {
        isOverWood = true
        updateCartSound()
    }
    
    func stopWood() {
        isOverWood = false
        updateCartSound()
    }
    
    var characterSpeed:Float = 1.0 {
        didSet {
            updateSpeed()
        }
    }
    
    var boostSpeedFactor:Float = 1.0 {
        didSet {
            updateSpeed()
        }
    }
    
    func trigger(characterSpeed speed:Float) {
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 2.0
        SCNTransaction.commit()
    }
    
    func triggerCollision() {
        guard squatCounter <= 0 else {return}
        //Play sound and animate
        character.runAction(.playAudio(hitSound, waitForCompletion:false))
        character.addAnimation(slapAnimation, forKey: nil)
        //Add Stars
        let emitter = character.childNode(withName: "Bip001_Head", recursively: true)
        emitter?.addParticleSystem(stars)
    }
    
    private func activateTriggers() {
        let characterPosition = float3(character.presentation.convertPosition(SCNVector3Zero, to: nil))
        var index = 0
        let didTrigger = false
        for trigger in triggers {
            if length_squared(characterPosition - trigger.position) < 0.05 {
                if activeTriggerIndex != index {
                    activeTriggerIndex = index
                    trigger.action(self)
                    break
                }
            }
            index += 1
        }
        if didTrigger == false {
            activeTriggerIndex = -1
        }
    }
    
    private func respawnCollectables() {
        for collectable in collectables.childNodes {
            collectable.categoryBitMask = 0
            collectable.scale = SCNVector3(x:1,y:1,z:1)
        }
        for collectable in speedItems.childNodes {
            collectable.categoryBitMask = 0
            collectable.scale = SCNVector3(x:1,y:1,z:1)
        }
    }
    
    private func collectItems() {
        let leftHandPosition = float3(leftHand.presentation.convertPosition(SCNVector3Zero, to: nil))
        let rightHandPosition = float3(rightHand.presentation.convertPosition(SCNVector3Zero, to: nil))
        for collectable in collectables.childNodes {
            guard collectable.categoryBitMask != Int(CollectableState.beingCollected.rawValue) else {continue}
            let collectablePosition = float3(collectable.position)
            if length_squared(leftHandPosition-collectablePosition) < 0.2 || length_squared(rightHandPosition - collectablePosition) < 0.2 {
                collectable.categoryBitMask = Int(CollectableState.beingCollected.rawValue)
                SCNTransaction.begin()
                SCNTransaction.animationDuration = 0.25
                collectable.scale = SCNVector3Zero
                scene.addParticleSystem(collectParticleSystem, transform: collectable.presentation.worldTransform)
                if let name = collectable.name,name.hasPrefix("big") {
                    headEmitter.addParticleSystem(collectParticleSystem)
                    collectable.runAction(.playAudio(collectSound2, waitForCompletion: false))
                    sceneView.didCollectBigItem()
                } else {
                    collectable.runAction(.playAudio(collectSound, waitForCompletion: false))
                    sceneView.didCollectItem()
                }
                SCNTransaction.commit()
                break
            }
        }
        
        for collectable in speedItems.childNodes {
            guard collectable.categoryBitMask != Int(CollectableState.beingCollected.rawValue) else {continue}
            let collectablePosition = float3(collectable.position)
            if length_squared(rightHandPosition - collectablePosition) < 0.2 {
                collectable.categoryBitMask = Int(CollectableState.beingCollected.rawValue)
                SCNTransaction.begin()
                SCNTransaction.animationDuration = 0.25
                collectable.scale = SCNVector3Zero
                collectable.runAction(.playAudio(collectSound2, waitForCompletion: false))
                scene.addParticleSystem(collectParticleSystem, transform: collectable.presentation.worldTransform)
                SCNTransaction.commit()
            
                SCNTransaction.begin()
                SCNTransaction.animationDuration = 1.0
                let pov = sceneView.pointOfView!
                pov.camera?.xFov = 100.0
                pov.camera?.motionBlurIntensity = 1.0
                let adjustCamera = SCNAction.run({ (node) in
                    SCNTransaction.begin()
                    SCNTransaction.animationDuration = 1.0
                    pov.camera?.xFov = 70
                    pov.camera?.motionBlurIntensity = 0.0
                    SCNTransaction.commit()
                })
                pov.runAction(.sequence([.wait(duration: 2.0),adjustCamera]))
                character.runAction(.playAudio(cartBoost, waitForCompletion: false))
                SCNTransaction.commit()
                break
            }
        }
    }
    func changeSpeedSound(speed:UInt) {
        railSoundSpeed = speed
        updateCartSound()
    }
    
    func updateCartSound() {
        guard isSoundEnabled else {return}
        wheels.removeAllAudioPlayers()
        switch railSoundSpeed {
        case _ where isOverWood:
            wheels.addAudioPlayer(SCNAudioPlayer(source: railWoodSound))
        case 1:
            wheels.addAudioPlayer(SCNAudioPlayer(source: railLowSpeedSound))
        case 3:
            wheels.addAudioPlayer(SCNAudioPlayer(source: railHighSpeedSound))
        case let speed where speed > 0:
            wheels.addAudioPlayer(SCNAudioPlayer(source: railMediumSpeedSound))
        default:
            break
        }
    }
    
    func updateSpeed() {
        let speed = characterSpeed * boostSpeedFactor
        let effectiveSpeed = CGFloat(speedFactor * speed)
        scene.rootNode.setAnimationSpeed(effectiveSpeed, forKey: cartAnimationName)
        wheels.setAnimationSpeed(effectiveSpeed, forKey: "wheel")
        idleAnimationOwner.setAnimationSpeed(effectiveSpeed, forKey: "bob_idle-1")
        updateCartSound()
    }
    
    func squat() {
        SCNTransaction.begin()
        SCNTransaction.completionBlock = {
            self.squatCounter -= 1
        }
        squatCounter += 1
        character.addAnimation(squatAnimation, forKey: nil)
        character.runAction(.playAudio(cartHide, waitForCompletion: false))
        SCNTransaction.commit()
    }
    
    func jump() {
        character.addAnimation(jumpAnimation, forKey: nil)
        character.runAction(.playAudio(cartJump, waitForCompletion: false))
    }
    
    func leanLeft() {
        character.addAnimation(leanLeftAnimation, forKey: nil)
        character.runAction(.playAudio(cartTurnLeft, waitForCompletion: false))
    }
    func leanRight() {
        character.addAnimation(leanRightAnimation, forKey: nil)
        character.runAction(.playAudio(cartTurnRight, waitForCompletion: false))
    }
    
    func startMusic() {
        guard isSoundEnabled else {return}
        let musicIntroSource = Assets.sound(named: "music_intro.mp3")
        let musicLoopSource = Assets.sound(named: "music_loop.mp3")
        musicLoopSource.loops = true
        musicIntroSource.isPositional = false
        musicLoopSource.isPositional = false
        
        musicIntroSource.shouldStream = false
        musicLoopSource.shouldStream = true
        
        (self.view as! SCNView).scene?.rootNode.runAction(.playAudio(musicIntroSource, waitForCompletion: true), completionHandler: { [unowned self] in
            self.sceneView.scene?.rootNode.addAudioPlayer(SCNAudioPlayer.init(source: musicLoopSource))
        })
    }
    
    func startGameIfNeeded()->Bool {
        guard gameState == .notStarted else {return false}
        sceneView.setup2DOverlay()
        sceneView.scene?.rootNode.removeAllAudioPlayers()
        startMusic()
        gameState = .started
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 2.0
        SCNTransaction.completionBlock = {
            self.jump()
        }
        
        let idleAnimation = Assets.animation(named: "animation-start.scn")
        character.addAnimation(idleAnimation, forKey: nil)
        character.removeAnimation(forKey: "start", fadeOutDuration: 0.3)
        sceneView.pointOfView = sceneView.scene?.rootNode.childNode(withName: "Camera", recursively: true)
        SCNTransaction.commit()
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 5.0
        characterSpeed = 1.0
        railSoundSpeed = 1
        SCNTransaction.commit()
        return true
    }
    func setupGameControllers() {
        let sceneView = self.view as! SCNView
        let dirctions:[UISwipeGestureRecognizerDirection] = [.left,.right,.up,.down]
        for dirction in dirctions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(BadgerViewController.didSwipe(g:)))
            gesture.direction = dirction
            sceneView.addGestureRecognizer(gesture)
        }
    }
    func didSwipe(g:UISwipeGestureRecognizer) {
        if startGameIfNeeded(){return}
        switch g.direction {
        case UISwipeGestureRecognizerDirection.up:jump()
        case UISwipeGestureRecognizerDirection.down:squat()
        case UISwipeGestureRecognizerDirection.left:leanLeft()
        case UISwipeGestureRecognizerDirection.right:leanRight()
        default:break
        }
    }
}
