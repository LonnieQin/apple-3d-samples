//
//  BadgerView.swift
//  MetalSamples
//
//  Created by lonnie on 2017/2/21.
//  Copyright © 2017年 Apple. All rights reserved.
//

import SceneKit
import SpriteKit
class BadgerView: SCNView {
    
    private let _overlayNode = SKNode()
    private let _scaleNode = SKNode()
    private let _collectedItemsCountLabel = SKLabelNode(fontNamed: "Superclarendon")
    
    private let _lifeLabel = SKLabelNode(fontNamed: "Superclarendon")
    
    private func update2DOverlays() {
        _overlayNode.position = CGPoint(x: 0.0, y: bounds.size.height)
    }
    
    #if os(iOS) || os(tvOS)
    override func layoutSubviews() {
        super.layoutSubviews()
        update2DOverlays()
    }
    #endif
    
    func setup2DOverlay() {
        let w = bounds.size.width
        let h = bounds.size.height
        
        //Setup the game overlays using SpriteKit
        let skScene = SKScene(size: CGSize(width: w, height: h))
        skScene.scaleMode = .resizeFill
        skScene.addChild(_scaleNode)
        _scaleNode.addChild(_overlayNode)
        _overlayNode.position = CGPoint(x: 0.0, y: h)
        
        //The Bob ICON
        let bobSprite = SKSpriteNode(imageNamed: "BobHUD.png")
        bobSprite.position = CGPoint(x: 70, y: -50)
        bobSprite.xScale = 0.5
        bobSprite.yScale = 0.5
        _overlayNode.addChild(bobSprite)
        
        _collectedItemsCountLabel.text = "x0"
        _collectedItemsCountLabel.horizontalAlignmentMode = .left
        _collectedItemsCountLabel.position = CGPoint(x: 135, y: -63)
        _overlayNode.addChild(_collectedItemsCountLabel)
        
        self.overlaySKScene = skScene
        skScene.isUserInteractionEnabled = false
        
        _lifeLabel.text = "x100"
        _lifeLabel.position = CGPoint(x: self.frame.size.width-120, y: -63)
        //_overlayNode.addChild(_lifeLabel)
    }
    
    var collectedItemsCount = 0 {
        didSet {
            _collectedItemsCountLabel.text = "x\(collectedItemsCount)"
            
        }
    }
    
    var life:Int = 100 {
        didSet {
            _lifeLabel.text = "life:\(life)"
        }
    }
    
    func didCollectItem() {
        collectedItemsCount += 1
    }
    
    func didCollectBigItem() {
        collectedItemsCount += 10
    }
    func didDamageLife() {
        life -= 1
    }
}
